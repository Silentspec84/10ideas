<?php

return [
    'Username' => 'Логин',
    'Email' => 'Почта',
    'Registration ip' => 'ip регистрации',
    'New email' => 'Новая почта',
    'Password' => 'Пароль',
    'Registration time' => 'Время регистрации',
    'Last login' => 'Последний вход',
    'Confirmation time' => 'Время подтверждения почты',
    'This username has already been taken' => 'Этот логин уже используется',
    'This email address has already been taken' => 'Эта почта уже используется',
    'Thank you, registration is now complete.' => 'Спасибо, регистрация завершена',
    'Something went wrong and your account has not been confirmed.' => 'Что-то пошло не так и ваш аккаунт не подтвержден',
    'The confirmation link is invalid or expired. Please try requesting a new one.' => 'Ссылка для подтверждения неверна или истекла. Пожалуйста, попробуйте запросить еще одну.',
    'Your confirmation token is invalid or expired' => 'Токен подтверждения неверен или истек',
    'An error occurred processing your request' => 'В процессе обработки вашего запроса возникла ошибка',
    'Awesome, almost there. Now you need to click the confirmation link sent to your old email address' =>
    'Замечательно, мы почти закончили. Вам осталось только кликнуть по ссылке подтверждения, высланной вам на старую почту',
    'Awesome, almost there. Now you need to click the confirmation link sent to your new email address' =>
    'Замечательно, мы почти закончили. Вам осталось только кликнуть по ссылке подтверждения, высланной вам на новую почту',
    'Your email address has been changed' => 'Ваша почта успешно изменена'
];