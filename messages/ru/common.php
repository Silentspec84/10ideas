<?php

return [
    'New idea' => 'Новая идея',
    'Today' => 'Сегодня',
    'Leaderboard' => 'Рейтинг',
    'All ideas' => 'Все идеи',
    'Login' => 'Вход',
    'Register' => 'Регистрация',
    'IdeaPoints' => 'Очки Идей',
    'CheckPoints' => 'Голоса',
    'Logout' => 'Выход',
    'Settings' => 'Настройки',
    'Sorry! This section is still in development!' =>
    'Извините, этот раздел все еще в разработке!',
    'SERVICE' => 'СЕРВИС',
    'Home page' => 'Главная',
    'Reviews' => 'Отзывы',
    'Additional features' => 'Дополнительные услуги',
    'Blog' => 'Блог',
    'HELP' => 'ПОМОШЬ',
    'Knowledge base' => 'База знаний',
    'Tutorials' => 'Руководство',
    'Creativity Trainer' => 'Тренажер креативности',
    'If you develop this creative skill, then for any problem you can come up with several solutions at once.' =>
    'Если вы разовьёте этот креативный навык, то для любой проблемы сможете придумать сразу несколько вариантов решения.',
    'You can say that the idea of inventing more than one solution to the problem is not new. Of course. But how often do you do this? How many ideas have you created for the last task you worked with? And how many ideas are there in your "idea bank"?' =>
    'Вы скажете, что идея придумывать больше одного варианта решения задачи не нова. Конечно. Но как часто вы это делаете? Сколько идей вы придумали для последней задачи, с которой работали? А сколько всего идей в вашем «идейном банке»?',
    'Try it, it`s free' => 'Попробуйте, это бесплатно',
    'Habbit to invent ideas' => 'Привычка генерировать идеи',
    'All you need is to start inventing 10 ideas a day and hold on for at least six months. Forming a sustainable habit of inventing 10 ideas a day can take you from 18 to 243 days.' =>
    'Всё, что вам нужно, — начать придумывать по 10 идей в день на любую тему и продержаться минимум шесть месяцев. Для формирования устойчивой привычки придумывать 10 идей в день вам может потребоваться от 18 до 243 дней.',
    'Creativity is a skill' => 'Креативность - это навык',
    'Creativity is a skill. It can be compared with sports. You will need a trainer, a program and regular workouts.' =>
    'Креативность - это навык. Это можно сравнить со спортом. Вам понадобится тренер, программа и регулярные тренировки.',
    'Why do you need to develop the skill' => 'Зачем вам развивать этот навык',
    'We offer you all the conditions for improving the skills of creativity absolutely for free' =>
    'Мы предлагаем вам все условия для совершенствования навыка креативности абсолютно бесплатно',
    '3,000 ideas in one year' => '3000 идей в год',
    'Each month you will have 300 or more new ideas, and in a year you will record more than 3,000.80% of the world\'s population does not appear as much in their entire life.' =>
    'Каждый месяц у вас будет 300 или больше новых идей, а за год вы запишете более 3 000. У 80% населения Земли за всю жизнь столько не появляется.',
    '10 ideas on the fly ability' => 'Способность генерировать 10 идей на лету',
    'For each life or work situation, you can come up with 10 ideas on the fly.And where there are several ideas, there is a choice and a solution.' =>
    'Для каждой жизненной или рабочей ситуации вы сможете с ходу придумать 10 идей. А где есть несколько идей, там есть выбор и решение.',
    '10 is not enough' => '10 не достаточно',
    'Over time you will realize that 10 is not enough, and you will begin to invent more, deriving pleasure from it.' =>
    'Со временем вы поймёте, что 10 — это мало, и станете придумывать больше, получая от этого удовольствие.',
    'Develop your "idea muscle"' => 'Качайте вашу "Идейную мышцу"',
    'You have no obligation to anyone! You can immediately forget what you recorded. Because the most important thing in this practice is to develop the skill and pump through the "idea muscle."' =>
    'У вас нет никаких обязательств ни перед кем! Вы сразу можете забыть то, что записали. Потому что самое важное в этой практике - развить навык и прокачать «идейную мышцу».',
    'A chance to invent cool idea' => 'Шанс придумать крутую идею',
    'But if suddenly you come up with a cool idea, you can try to develop it. It can bring you happiness, joy and even money. And you need to do just one step.' =>
    'Но если вдруг вы придумаете классную идею, можете попробовать её развить. Она может принести вам счастье, радость и даже деньги. И вам для этого нужно сделать всего один шаг.',
    'Motivation' => 'Мотивация',
    'In the first week and a half you will be very interested, then the week of the "loss of the fuse" will come - this is the most difficult time, and you need to find motivation. The best motivation is social participation and approval. Show what you make up and see what others make up.' =>
    'В первую неделю-полторы вам будет очень интересно, затем наступит неделя «потери запала» - это самое сложное время, и вам нужно найти мотивацию. Лучшая мотивация - социальное участие и одобрение. Показывайте, что придумываете, и смотрите, что придумывают другие.',
    'Today`s topic is ' => 'Тема дня сегодня - ',
    'recreation' => 'отдых, путешествия, хобби',
    'business' => 'бизнес проекты',
    'efficiency' => 'эффективность',
    'self development' => 'саморазвитие',
    'health' => 'здоровье',
    'work' => 'работа',
    'relationship' => 'отношения',
    'Formulate and write down your idea in the form below. Be brief - the idea should not be longer than 255 characters. With each new publication of your idea, you spend 1 ideapoint. Their current number can be seen in the top menu on the right. You can fill in idepoints only by voting for other people\'s ideas. Act wisely!' =>
    'Сформулируйте и напишите вашу идею в форме ниже. Будьте кратки - идея не должна быть длиннее 255 символов. С каждой новой публикацией вашей идеи вы тратите 1 Очко Идеи. Их точное количество можно увидеть в меню справа сверху. Вы можете получить новые Очки Идей, голосуя за чужие идеи на странице "Сегодня". Действуйте с умом!',
    'Post idea' => 'Публиковать',
    'Below you will find the ideas of all users for today. You can vote for any - raise or lower the rating. For each vote, one Voice will be written off and half of the Idea Point added. Every day the number of points Voice increases by 10. And now support the most original idea for today!' =>
    'Ниже вы найдёте идеи всех пользователей за сегодня. Вы можете проголосовать за любую - поднять или же опустить рейтинг. За каждое голосование будет списано по одному Голосу и добавлено по половине Очка Идеи. Каждый день количество очков Голоса увеличивается на 10. Ну а теперь поддержите самую оригинальную идею за сегодня!',
    'Your idea was posted, but you spent all IdeaPoints for today! You can vote for ideas of other users and earn some more IdeaPoints.' =>
    'Ваша идея сохранена, но вы потратили все Очки Идей на сегодня! Вы все еще можете проголосовать за идеи других пользователей и получить за это еще Очки Идей.',
    'Your idea was posted, but you spent all IdeaPoints for today! See you tomorrow.' =>
    'Ваша идея сохранена, но вы потратили все Очки Идей на сегодня! Приходите завтра.',
    'You spent all CheckPoints for today! Try it tomorrow.' =>
    'Вы потратили все Очки Голосов на сегодня! Приходите завтра.',
    'You have already checked this idea!' =>
    'Вы уже проголосовали за эту идею!',
    'Nice try, but you can`t voute for your idea!' =>
    'Неплохая попытка, но вы не можете голосовать за собственные идеи!',
    'Sign in' => 'Вход',
    'Password' => 'Пароль',
    'Forgot password?' => 'Забыли пароль?',
    'Didn\'t receive confirmation message?' => 'Не получили подтверждающего письма?',
    'Don\'t have an account? Sign up!' => 'Нет аккаунта? Зарегистрируйтесь!',
    'Voted' => 'Голос отдан',
    'Below you will find all ideas of all users. You can vote for this ideas too! There are already:' =>
    'Ниже вы найдете все идеи всех пользователей за все время. За них вы тоже можете проголосовать! Их уже:',
    'Only mine' => 'Только мои',
    'All' => 'Все',
    'Other users' => 'Другие пользователи',
    'Below you can find rating of ideas of users. The more users likes the idea, the higher its rating.' => 'Ниже вы найдете рейтинг идей пользователей. Чем больше лайков у идеи, тем выше ее рейтинг.',
    'Leader: ' => 'Лидер: ',
    'place' => 'место',
    'You have:' => 'У вас осталось:',
    'Idea Points' => 'Очков Идей',
    'You still have some CheckPoints:' => 'У вас еще остались голоса:',
    '. Rate other users ideas and you`ll get' => '. Голосуйте за чужие идеи и получите',
    'additional IdeaPoints!' => 'дополнительные Очки Идей!',
    'My ideas' => 'Мои идеи',
    'Today posted ideas:' => 'Идей сегодня опубликовано:',
    'All ideas of user' => 'Все идеи пользователя',
    'Below you can find all ideas of user' => 'Ниже вы найдете все идеи пользователя',
    'for all time. It`s near' => 'за все время. Это около',
    'Sorry!' => 'Простите!',
    'Warning!' => 'Внимание!',
    'You have some errors in your idea form!' => 'В вашей форме ошибки! Она не будет отправлена.',
    'Nice try!' => 'Хитро!',
    'Congratulations!' => 'Поздравляем!',
    'You just posted your idea!' => 'Вы успешно опубликовали вашу идею!',
    'Go to Today page and rate' => 'Перейти на страницу "Сегодня" и голосовать',
    'User Ideas' => 'Идеи пользователя',
    'Go to user ideas' => 'Перейти к идеям пользователя',
    'Add to favourites' => 'Добавить в избранное',
    'Edit idea' => 'Редактировать',
    'Comments:' => 'Комментариев:',
    'This is your IdeaPoints. Everyday you earn +5 IdeaPoints. You need them to post new ideas' =>
    'Это ваши Очки Идей. Каждый день вы получаете +5 Очков Идей. Они нужны вам, чтобы публиковать новые идеи',
    'This is your CheckPoints. Everyday you earn +10 CheckPoints. You need them to vote other user`s ideas. For every 2 spent CheckPoints one IdeaPoint is adding' =>
    'Это ваши Голоса. Каждый день вы получаете +10 Голосов. Они нужны вам, чтобы голосовать за идеи других пользователей. За каждые два потраченных голоса добавляется 1 Очко Идеи.',
    'Save' => 'Сохранить',
    'Your idea was edited sucessfully!' => 'Ваша идея была успешно изменена!',
    'To user page' => 'На страницу автора',
    'Favourites' => 'Избранное'
    ];
