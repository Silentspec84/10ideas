<?php

namespace app\classes\processors;

use app\models\Comment;
use app\models\User;
use yii\helpers\ArrayHelper;

class CommentProcessor
{
    private $user;

    public function __construct($user_id = null)
    {
        if ($user_id !== null) {
            $this->user = User::findOne($user_id);
        } else {
            $this->user = null;
        }
    }

    public static function getComments($idea_id, $users, $users_settings)
    {
        $all_comments_request = Comment::find()->where(['idea_id' => $idea_id]);

        $all_comments = $all_comments_request->all();
        $all_comments_count = $all_comments_request->count();
        $comments = [];

        /** @var Comment $comment */
        foreach ($all_comments as $comment) {
            $comment->getTranslate();
            $comments[] = ['comment' => $comment, 'user' => $users[$comment->user_id], 'user_settings' => $users_settings[$comment->user_id]];
        }

        $comments_data = [
            'comments' => $comments,
            'count' => $all_comments_count
        ];

        return $comments_data;
    }

}