<?php

namespace app\classes\processors;

use app\models\Idea;
use app\models\Lang;
use app\models\PostedIdea;
use app\models\User;
use app\models\UserFavourites;
use app\models\UserSettings;
use Yii;
use yii\helpers\ArrayHelper;
use app\classes\processors\CommentProcessor;

class IdeaProcessor
{
    private $user;

    public function __construct($user_id = null)
    {
        if ($user_id !== null) {
            $this->user = User::findOne($user_id);
        } else {
            $this->user = null;
        }
    }

    public function getIdeas($page = null, $timestamp_from = null, $timestamp_till = null, $order_attibute = null, $sort = null, $user_id = null, $fav = false)
    {
        $page = $page === null ? 1 : $page;
        $items_on_page = 10;
        $offset = ($page - 1) * $items_on_page;
        if (!$fav) {
            $all_ideas_request = Idea::find()->limit($items_on_page)->offset($offset);
        } else {
            $all_ideas_request = Idea::find();
        }

        if ($order_attibute !== null) {
            $all_ideas_request = $all_ideas_request->orderBy([$order_attibute => $sort]);
        }
        if ($timestamp_from !== null) {
            $all_ideas_request = $all_ideas_request->andWhere(['>=', 'date_created', $timestamp_from]);
        }
        if ($timestamp_till !== null) {
            $all_ideas_request = $all_ideas_request->andWhere(['<=', 'date_created', $timestamp_till]);
        }
        if ($user_id !== null) {
            $all_ideas_request = $all_ideas_request->andWhere(['user_id' => $user_id]);
        }

        $all_ideas = $all_ideas_request->all();
        $all_ideas_count = $all_ideas_request->count();

        $posted_ideas = PostedIdea::find()->all();
        $users = User::find()->all();
        $users = ArrayHelper::index($users, 'id');
        $users_settings = UserSettings::find()->all();
        $users_settings = ArrayHelper::index($users_settings, 'user_id');
        $languages = Lang::find()->all();
        $languages = ArrayHelper::index($languages, 'id');
        if ($fav) {
            $favs = UserFavourites::find()->where(['user_id' => $this->user->id])->all();
            $favs = ArrayHelper::index($favs, 'idea_id');
        }
        $ideas = [];
        $voted_users = [];

        /** @var Idea $idea */
        foreach ($all_ideas as $idea) {
            if ($fav && empty($favs[$idea->id])) {
                continue;
            }
            $voted = false;
            /* @var PostedIdea $posted_idea */
            foreach ($posted_ideas as $posted_idea) {
                if ($posted_idea->idea_id !== $idea->id) {
                    continue;
                }
                if (!empty($this->user) && $posted_idea->user_id === $this->user->id) {
                    $voted = true;
                }
                $voted_users[$idea->id][$posted_idea->user_id] = $users[$posted_idea->user_id];
            }
            $idea->getTranslate();
            $comments = CommentProcessor::getComments($idea->id, $users, $users_settings);
            $ideas[] = ['idea' => $idea, 'comments' => $comments ,'voted_users' => $voted_users, 'voted' => $voted, 'user_settings' => $users_settings[$idea->user_id], 'user' => $users[$idea->user_id], 'lang' => $languages[$users_settings[$idea->user_id]['lang_id']]];
        }

        $ideas_data = [
            'ideas' => $ideas,
            'count' => $all_ideas_count
        ];

        return $ideas_data;
    }

}