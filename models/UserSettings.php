<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "lang".
 *
 * @property int $id
 * @property int $user_id
 * @property string $avatar
 * @property int $idea_points
 * @property int $check_points
 * @property int $today_points
 * @property int lang_id
 */
class UserSettings extends ActiveRecord
{

    public static function tableName()
    {
        return 'user_settings';
    }

    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'check_points', 'today_points', 'lang_id'], 'integer'],
            [['idea_points'], 'double'],
            [['avatar'], 'string', 'max' => 255],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'user_id',
            'avatar' => 'avatar',
        ];
    }

    public function addPoints()
    {
        /** Получаем настройки */
        $days_after_last_points_adding = $this->getDaysAfterLastPointsAdding();
        if ($days_after_last_points_adding > 0) {
            $this->idea_points = $this->idea_points + $days_after_last_points_adding * 5;
            $this->check_points = $this->check_points + $days_after_last_points_adding * 10;
            $this->today_points = time();
            $this->save();
            return;
        }
        return;
    }

    private function getDaysAfterLastPointsAdding()
    {
        $date = date('d.m.Y', $this->today_points);
        $date = strtotime($date);
        $diff = (int)floor((time() - $date) / 86400); // целых дней в разнице
        return $diff;
    }

    public function setUserLang()
    {
        $this->lang_id = Lang::getCurrent()->id;
        $this->save();
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

}
