<?php

namespace app\models;

use dektrium\user\models\User as BaseUser;
use Yii;

class User extends BaseUser
{
    public function beforeSave($insert)
    {
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        $settings = UserSettings::find()->where(['user_id' => $this->id])->one();
        if(!$settings) {
            $settings = new UserSettings();
            $settings->user_id = $this->id;
            $settings->avatar = '../../web/img/avatar/ava.jpg';
            $settings->idea_points = 5;
            $settings->check_points = 10;
            $settings->today_points = time();
            $settings->lang_id = Lang::getCurrent()->id;
            $settings->save();
        }
        return parent::afterSave($insert, $changedAttributes);
    }
}