<?php

namespace app\models;

use Yandex\Translate\Exception;
use Yandex\Translate\Translator;
use Yii;

/**
 * This is the model class for table "lang".
 *
 * @property int $id
 * @property string $url
 * @property string $local
 * @property string $name
 * @property string $img_path
 * @property int $default
 * @property int $date_update
 * @property int $date_create
 */
class Lang extends \yii\db\ActiveRecord
{

    /**
     * Переменная, для хранения текущего объекта языка
     */
    static $current = null;

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'lang';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['date_create', 'date_update'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['date_update'],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['url', 'local', 'name', 'img_path', 'date_update', 'date_create'], 'required'],
            [['default', 'date_update', 'date_create'], 'integer'],
            [['url', 'local', 'name', 'img_path'], 'string', 'max' => 255],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'Url',
            'local' => 'Local',
            'name' => 'Name',
            'img_path' => 'Path to flag image',
            'default' => 'Default',
            'date_update' => 'Date Update',
            'date_create' => 'Date Create',
        ];
    }

    /**
     * Получение текущего объекта языка
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getCurrent()
    {
        if (self::$current === null) {
            $lang_obj = self::getUserLang(Yii::$app->user->id);
            if (!empty($lang_obj)) {
                $lang = $lang_obj->url;
            } else {
                if (($cookie = Yii::$app->request->cookies->get('lang')) !== null) {
                    $lang = $cookie->value;
                } else {
                    $lang = self::getLangByIp();
                }
            }
            $language = Lang::find()->where('url = :url', [':url' => $lang])->one();
            self::$current = $language === null ? self::getDefaultLang() : $language;
        }
        return self::$current;
    }

    /**
     * Получения объекта языка по умолчанию
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getDefaultLang()
    {
        return Lang::find()->where('`default` = :default', [':default' => 1])->one();
    }

    static function getLangByIp()
    {
        preg_match_all('/([a-z]{1,8}(?:-[a-z]{1,8})?)(?:;q=([0-9.]+))?/', strtolower($_SERVER["HTTP_ACCEPT_LANGUAGE"]), $matches); // вычисляем соответствия с массивом $matches<br >
        $langs = array_combine($matches[1], $matches[2]); // Создаём массив с ключами $matches[1] и значениями $matches[2]<br >
        foreach ($langs as $n => $v) {
            $langs[$n] = $v ? $v : 1; // Если нет q, то ставим значение 1<br >
        }
        arsort($langs); // Сортируем по убыванию q<br >

        return substr(key($langs),0,2);
    }

    public static function getUserLang($user_id)
    {
        $settings = UserSettings::find()->where(['user_id' => $user_id])->one();
        if (!$settings) {
            return null;
        }
        return Lang::find()->where(['id' => $settings->lang_id])->one();
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }
}
