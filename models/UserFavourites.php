<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "lang".
 *
 * @property int $id
 * @property int $user_id
 * @property int $idea_id
 * @property string $group_title
 * @property int $date_created
 */

class UserFavourites extends ActiveRecord
{

    public static function tableName()
    {
        return 'user_favourites';
    }

    public function rules()
    {
        return [
            [['user_id', 'idea_id', 'group_title'], 'required'],
            [['user_id', 'idea_id'], 'integer'],
            [['group_title'], 'string', 'max' => 255],
        ];
    }

    public static function getUserFavouriteGroups()
    {
        $all_groups = UserFavourites::find()->where(['user_id' => Yii::$app->user->id])->all();
        return ArrayHelper::index($all_groups, 'group_title');
    }

    public static function isIdeaInUserFavouriteGroups($idea_id)
    {
        $group = UserFavourites::find()->where(['user_id' => Yii::$app->user->id, 'idea_id' => $idea_id])->all();
        return !empty($group);
    }

    public static function getIdeaFavouriteGroup($idea_id)
    {
        return UserFavourites::find()->where(['user_id' => Yii::$app->user->id, 'idea_id' => $idea_id])->one();
    }
}