<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "lang".
 *
 * @property int $id
 * @property int $user_id
 * @property int $comment_id
 * @property int $date_created
 */
class UserComment extends ActiveRecord
{

    public static function tableName()
    {
        return 'user_comments';
    }

    public function rules()
    {
        return [
            [['user_id', 'comment_id'], 'required'],
            [['user_id', 'comment_id'], 'integer'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'user id',
            'comment_id' => 'comment id',
        ];
    }
}
