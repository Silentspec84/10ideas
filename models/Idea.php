<?php

namespace app\models;

use Yandex\Translate\Exception;
use Yandex\Translate\Translator;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "lang".
 *
 * @property int $id
 * @property int $user_id
 * @property string $text
 * @property int $sector_id
 * @property int $status
 * @property int $rating
 * @property int $date_created
 */
class Idea extends ActiveRecord
{
    private $key = 'trnsl.1.1.20190210T152829Z.c23e3657eae974fb.c4462cbe16429c8709720765cfc9633a82d92a66';

    private $cache_key;

    public static $sector = [
        0 => ['title' => 'recreation', 'img' => '../../web/img/sectors/recreation.jpg'],            // отдых
        1 => ['title' => 'business', 'img' => '../../web/img/sectors/business.jpg'],                // бизнес
        2 => ['title' => 'efficiency', 'img' => '../../web/img/sectors/efficiency.jpg'],            // эффективность
        3 => ['title' => 'self development', 'img' => '../../web/img/sectors/development.jpg'],     // саморазвитие
        4 => ['title' => 'health', 'img' => '../../web/img/sectors/health.jpg'],                    // здоровье
        5 => ['title' => 'work', 'img' => '../../web/img/sectors/work.jpg'],                        // работа
        6 => ['title' => 'relationship', 'img' => '../../web/img/sectors/relationship.jpg'],        // отношения
    ];

    public static function tableName()
    {
        return 'ideas';
    }

    public function rules()
    {
        return [
            [['user_id', 'sector_id', 'text'], 'required'],
            [['user_id', 'sector_id', 'status', 'date_created', 'rating'], 'integer'],
            [['text'], 'string', 'max' => 255, 'message' => 'Your idea must be maximum 255 symbols. Keep it brief!'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'user_id',
            'avatar' => 'avatar',
        ];
    }

    public function beforeSave($insert)
    {
        if($this->isNewRecord) {
            $this->date_created = time();
            $this->text = strip_tags($this->text);
        }

        return parent::beforeSave($insert);
    }

    public function afterFind()
    {
        $this->text = strip_tags($this->text);
        parent::afterFind();
    }

    public function getTranslate()
    {
        $this->cache_key = $this->id . '-' . $this->user_id . '-' . Lang::getCurrent()->url;
        $text = Yii::$app->cache->get($this->cache_key);
        if ($text) {
            $this->text = strip_tags($text);
        } else {
            $translator = new Translator($this->key);
            try {
                $this->text = $translator->translate($this->text, $translator->detect($this->text) . '-' . Lang::getCurrent()->url, true);
                Yii::$app->cache->set($this->cache_key, $this->text);
            } catch (Exception $e) {

            }
        }
    }

}
