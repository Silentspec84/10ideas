<?php

namespace app\models;

use Yandex\Translate\Exception;
use Yandex\Translate\Translator;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "lang".
 *
 * @property int $id
 * @property int $user_id
 * @property int $idea_id
 * @property string $text
 * @property int $rating
 * @property int $date_created
 */

class Comment extends ActiveRecord
{

    private $key = 'trnsl.1.1.20190210T152829Z.c23e3657eae974fb.c4462cbe16429c8709720765cfc9633a82d92a66';

    private $cache_key;

    public static function tableName()
    {
        return 'comments';
    }

    public function rules()
    {
        return [
            [['user_id', 'idea_id', 'text', 'rating'], 'required'],
            [['user_id', 'idea_id', 'rating', 'date_created'], 'integer'],
            [['text'], 'string', 'max' => 1000],
        ];
    }

    public function beforeSave($insert)
    {
        if($this->isNewRecord) {
            $this->date_created = time();
            $this->text = strip_tags($this->text);
        }

        return parent::beforeSave($insert);
    }

    public function getTranslate()
    {
        $this->cache_key = 'comment-' . $this->id . '-' . $this->user_id . '-' . Lang::getCurrent()->url;
        $text = Yii::$app->cache->get($this->cache_key);
        if ($text) {
            $this->text = strip_tags($text);
        } else {
            $translator = new Translator($this->key);
            try {
                $this->text = $translator->translate($this->text, $translator->detect($this->text) . '-' . Lang::getCurrent()->url, true);
                Yii::$app->cache->set($this->cache_key, $this->text);
            } catch (Exception $e) {

            }
        }
    }

}