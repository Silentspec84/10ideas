<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "lang".
 *
 * @property int $id
 * @property int $user_id
 * @property int $idea_id
 */
class PostedIdea extends ActiveRecord
{

    public static function tableName()
    {
        return 'posted_ideas';
    }

    public function rules()
    {
        return [
            [['user_id', 'idea_id'], 'required'],
            [['user_id', 'idea_id'], 'integer'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'user_id',
            'idea_id' => 'idea id',
        ];
    }
}
