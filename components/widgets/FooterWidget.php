<?php

namespace app\components\widgets;

use yii\base\ErrorException;
use yii\base\ExitException;
use yii\base\Widget;

/**
 * Виджет отвечает за вывод кастомного футера
 */
class FooterWidget extends Widget
{

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('footer');
    }
}