<?php

namespace app\components\widgets;

use app\models\Lang;
use app\models\UserSettings;
use Yii;
use yii\base\Widget;

class LangWidget extends Widget
{
    public function init(){}

    public function run() {
        if (!Yii::$app->user->isGuest) {
            $language_id = UserSettings::find()->where(['user_id' => Yii::$app->user->id])->one()->lang_id;
            $language = Lang::find()->where(['id' => $language_id])->one();
        } else {
            $language = Lang::getCurrent();
        }

        return $this->render('lang', [
            'current' => $language,
            'langs' => Lang::find()->where('id != :current_id', [':current_id' => $language->id])->all(),
        ]);
    }
}