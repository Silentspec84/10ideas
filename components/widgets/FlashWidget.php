<?php

namespace app\components\widgets;

use Yii;
use yii\base\Widget;

/**
 * Class FlashWidget
 * Виджет для отображение yii флеш-сообщений
 * Наиболее частое применение - сообщение об успешном сохранение данных.
 * Пример: FlashWidget::widget(['key_flash' => 'success', 'style' => FlashWidget::TYPE_SUCCESS]);
 * @package app\components
 */
class FlashWidget extends Widget
{

    const TYPE_DANGER = 'danger';
    const TYPE_WARNING = 'warning';
    const TYPE_SUCCESS = 'success';
    const TYPE_PRIMARY = 'primary';
    const TYPE_SECONDARY = 'secondary';
    const TYPE_LIGHT = 'light';

    /** @var string */
    public $key_flash;
    /** @var string */
    public $style;
    /** @var string */
    protected $flash_message;

    public function init()
    {
        parent::init();
        if (!$this->key_flash) {
            return '';
        }
        if (!$this->style) {
            $this->style = self::TYPE_SECONDARY;
        }
        $this->flash_message = Yii::$app->session->getFlash($this->key_flash);
    }

    public function run()
    {
        if ($this->flash_message && Yii::$app->session->hasFlash($this->key_flash)) {
            return '<div class="alert alert-'.$this->style.' alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>'
                .$this->flash_message .
                '</div>';
        }

        return '';
    }
}