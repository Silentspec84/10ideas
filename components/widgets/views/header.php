<?php

use app\components\widgets\LangWidget;
use app\models\UserSettings;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

/** @var UserSettings $settings */

$settings = UserSettings::find()->where(['user_id' => Yii::$app->user->id])->one();

?>

<nav class="uk-navbar uk-navbar-container" uk-navbar>
    <div class="uk-navbar-left">
        <a class="uk-navbar-item uk-logo" href="/"><img src="/../../web/img/logo2.png" alt="" height="5"></a>
    </div>
    <div class="uk-navbar-right">
        <ul class="uk-navbar-nav">

        </ul>
    </div>
    <div class="uk-navbar-left">
        <a class="uk-navbar-toggle uk-hidden@m" uk-toggle="target: #sidenav" uk-navbar-toggle-icon></a>
        <ul class="uk-navbar-nav uk-visible@m">

            <li class="uk-navbar-item uk-box-shadow-hover-medium <?php if(Yii::$app->controller->id === '/new'):?>uk-active<?php endif;?>">
                <a class="uk-button uk-button-text" href="/new"><?=Yii::t('common', 'New idea')?></a>
            </li>
            <li class="uk-navbar-item uk-box-shadow-hover-medium <?php if(Yii::$app->controller->id === '/userideas'):?>uk-active<?php endif;?>">
                <a class="uk-button uk-button-text" href="/userideas"><?=Yii::t('common', 'My ideas')?></a>
            </li>
            <li class="uk-navbar-item uk-box-shadow-hover-medium <?php if(Yii::$app->controller->id === '/today'):?>uk-active<?php endif;?>">
                <a class="uk-button uk-button-text" href="/today"><?=Yii::t('common', 'Today')?></a>
            </li>
<!--            <li class="uk-navbar-item uk-box-shadow-hover-medium --><?php //if(Yii::$app->controller->id === '/leaderboard'):?><!--uk-active--><?php //endif;?><!--">-->
<!--                <a class="uk-button uk-button-text" href="/leaderboard">--><?//=Yii::t('common', 'Leaderboard')?><!--</a>-->
<!--            </li>-->
            <li class="uk-navbar-item uk-box-shadow-hover-medium <?php if(Yii::$app->controller->id === '/favourites'):?>uk-active<?php endif;?>">
                <a class="uk-button uk-button-text" href="/favourites"><?=Yii::t('common', 'Favourites')?></a>
            </li>
            <li class="uk-navbar-item uk-box-shadow-hover-medium <?php if(Yii::$app->controller->id === '/all'):?>uk-active<?php endif;?>">
                <a class="uk-button uk-button-text" href="/all"><?=Yii::t('common', 'All ideas')?></a>
            </li>
        </ul>

    </div>
    <div class="uk-navbar-right uk-visible@m">
        <ul class="uk-navbar-nav">
            <?php if (Yii::$app->user->isGuest): ?>
                <li class="uk-navbar-item uk-box-shadow-hover-medium <?php if(Yii::$app->controller->id === '/user/security/login'):?>uk-active<?php endif;?>">
                    <a class="uk-button uk-button-text" href="/user/security/login"><?=Yii::t('common', 'Login')?></a>
                </li>
                <li class="uk-navbar-item uk-box-shadow-hover-medium <?php if(Yii::$app->controller->id === '/user/registration/register'):?>uk-active<?php endif;?>">
                    <a class="uk-button uk-button-text" href="/user/registration/register"><?=Yii::t('common', 'Register')?></a>
                </li>
            <?php else: ?>
                <li class="uk-navbar-item" style="color: #fca326">
                    <span class="uk-margin-small-right"
                          uk-icon="icon: info; ratio: 2"
                          uk-tooltip="<?=Yii::t('common', 'This is your IdeaPoints. Everyday you earn +5 IdeaPoints. You need them to post new ideas')?>">
                    </span>
                    <span class="uk-text-bold"><?=Yii::t('common', 'IdeaPoints')?>: <span id="idea-points"><?=$settings->idea_points?></span></span>
                </li>
                <li class="uk-navbar-item" style="color: #fca326">
                    <span class="uk-margin-small-right"
                          uk-icon="icon: warning; ratio: 2"
                          uk-tooltip="<?=Yii::t('common', 'This is your CheckPoints. Everyday you earn +10 CheckPoints. You need them to vote other user`s ideas. For every 2 spent CheckPoints one IdeaPoint is adding')?>">
                    </span>
                    <span class="uk-text-bold"><?=Yii::t('common', 'CheckPoints')?>: <span id="check-points"><?=$settings->check_points?></span></span>
                </li>
                <?php $form = ActiveForm::begin([
                    'id'                     => 'logout-form',
                    'enableAjaxValidation'   => true,
                    'enableClientValidation' => false,
                    'action' => Yii::$app->urlManager->createUrl('/user/security/logout'),
                ]); ?>
                <li class="uk-navbar-item <?php if(Yii::$app->controller->id === '/all'):?>uk-active<?php endif;?>">
                    <button class="uk-button uk-button-text uk-text-center uk-navbar-item uk-box-shadow-hover-medium"><?=Yii::t('common', 'Logout')?></button>
                </li>
                <?php ActiveForm::end(); ?>
<!--                <li class="uk-navbar-item uk-box-shadow-hover-medium --><?php //if(Yii::$app->controller->id === '/user/settings/profile'):?><!--uk-active--><?php //endif;?><!--">-->
<!--                    <a class="uk-button uk-button-text" href="/user/settings/profile">--><?//=Yii::t('common', 'Settings')?><!--</a>-->
<!--                </li>-->
            <?php endif; ?>
            <?= LangWidget::widget();?>
        </ul>
    </div>
</nav>

<div id="sidenav" uk-offcanvas="flip: true" class="uk-offcanvas-slide uk-dark uk-background-muted uk-margin-xlarge-bottom@s">
    <div class="uk-offcanvas-bar uk-margin-xlarge-bottom@s">
        <div class="">
            <ul class="uk-nav">
                <li class="uk-navbar-item uk-box-shadow-hover-medium <?php if(Yii::$app->controller->id === '/new'):?>uk-active<?php endif;?>">
                    <a style="color: #fca326" class="uk-button uk-button-text" href="/new"><?=Yii::t('common', 'New idea')?></a>
                </li>
                <li class="uk-navbar-item uk-box-shadow-hover-medium <?php if(Yii::$app->controller->id === '/userideas'):?>uk-active<?php endif;?>">
                    <a style="color: #fca326" style="color: #fca326" class="uk-button uk-button-text" href="/userideas"><?=Yii::t('common', 'My ideas')?></a>
                </li>
                <li class="uk-navbar-item uk-box-shadow-hover-medium <?php if(Yii::$app->controller->id === '/today'):?>uk-active<?php endif;?>">
                    <a style="color: #fca326" class="uk-button uk-button-text" href="/today"><?=Yii::t('common', 'Today')?></a>
                </li>
<!--                <li class="uk-navbar-item uk-box-shadow-hover-medium --><?php //if(Yii::$app->controller->id === '/leaderboard'):?><!--uk-active--><?php //endif;?><!--">-->
<!--                    <a style="color: #fca326" class="uk-button uk-button-text" href="/leaderboard">--><?//=Yii::t('common', 'Leaderboard')?><!--</a>-->
<!--                </li>-->
                <li class="uk-navbar-item uk-box-shadow-hover-medium <?php if(Yii::$app->controller->id === '/favourites'):?>uk-active<?php endif;?>">
                    <a style="color: #fca326" class="uk-button uk-button-text" href="/favourites"><?=Yii::t('common', 'Favourites')?></a>
                </li>
                <li class="uk-navbar-item uk-box-shadow-hover-medium <?php if(Yii::$app->controller->id === '/all'):?>uk-active<?php endif;?>">
                    <a style="color: #fca326" class="uk-button uk-button-text" href="/all"><?=Yii::t('common', 'All ideas')?></a>
                </li>
                <?php if (Yii::$app->user->isGuest): ?>
                    <li class="uk-navbar-item uk-box-shadow-hover-medium <?php if(Yii::$app->controller->id === '/user/security/login'):?>uk-active<?php endif;?>">
                        <a style="color: #fca326" class="uk-button uk-button-text" href="/user/security/login"><?=Yii::t('common', 'Login')?></a>
                    </li>
                    <li class="uk-navbar-item uk-box-shadow-hover-medium <?php if(Yii::$app->controller->id === '/user/registration/register'):?>uk-active<?php endif;?>">
                        <a style="color: #fca326" class="uk-button uk-button-text" href="/user/registration/register"><?=Yii::t('common', 'Register')?></a>
                    </li>
                <?php else: ?>
                    <li class="uk-navbar-item" style="color: #fca326">
                        <span class="uk-margin-small-right" uk-icon="icon: info; ratio: 2"></span><span class="uk-text-bold"><?=Yii::t('common', 'IdeaPoints')?>: <span id="my-idea-points"><?=$settings->idea_points?></span></span>
                    </li>
                    <li class="uk-navbar-item" style="color: #fca326">
                        <span class="uk-margin-small-right" uk-icon="icon: warning; ratio: 2"></span><span class="uk-text-bold""><?=Yii::t('common', 'CheckPoints')?>: <span id="my-check-points"><?=$settings->check_points?></span></span>
                    </li>
                    <?php $form = ActiveForm::begin([
                        'id'                     => 'logout-form',
                        'enableAjaxValidation'   => true,
                        'enableClientValidation' => false,
                        'action' => Yii::$app->urlManager->createUrl('/user/security/logout'),
                    ]); ?>
                    <li class="uk-navbar-item <?php if(Yii::$app->controller->id === '/all'):?>uk-active<?php endif;?>">
                        <button style="color: #fca326" class="uk-button uk-button-text uk-text-center uk-navbar-item uk-box-shadow-hover-medium"><?=Yii::t('common', 'Logout')?></button>
                    </li>
                    <?php ActiveForm::end(); ?>
<!--                    <li class="uk-navbar-item uk-box-shadow-hover-medium --><?php //if(Yii::$app->controller->id === '/user/settings/profile'):?><!--uk-active--><?php //endif;?><!--">-->
<!--                        <a style="color: #fca326" class="uk-button uk-button-text" href="/user/settings/profile">--><?//=Yii::t('common', 'Settings')?><!--</a>-->
<!--                    </li>-->
                <?php endif; ?>
                <span style="color: #fca326"><?= LangWidget::widget();?></span>
            </ul>
        </div>
    </div>
</div>