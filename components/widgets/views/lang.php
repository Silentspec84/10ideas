<?php

use app\models\Lang;
use yii\helpers\Html;

echo Html :: csrfMetaTags();

/** @var array $current */
/** @var array $langs */
/** @var Lang $lang */
?>

<li class="uk-navbar-item">
    <a class="uk-button uk-button-text">
        <img src="/<?=$current->img_path?>" style="height:10px;" class="uk-margin-small-right"> <span id="lang_name" style="color: #333"><?=$current->name;?></span>
    </a>
    <div class="uk-navbar-dropdown uk-width-1-6" uk-dropdown="pos: bottom-right; offset: 30; animation: uk-animation-slide-top-small; duration: 400">
        <ul class="uk-nav uk-navbar-dropdown-nav">
            <?php foreach ($langs as $lang):?>
                <li>
                    <span id="lang" onclick="language(<?=$lang->id?>)">
                        <img src="/<?=$lang->img_path?>" style="height:10px;">
                        <a style="color: #333" class="uk-button uk-button-text"><?=$lang->name?></a>
                    </span>
                </li>
            <?php endforeach;?>
        </ul>
    </div>
</li>

<script type="text/javascript">
    $('#lang').click(language);
    function language(id){
        var csrfToken = $('meta[name="csrf-token"]').attr("content");
        $.ajax({
                type: 'POST',
                url: '/main/lang',
                data: {id: id, _csrf : csrfToken}
            }
        )
            .done (function(data) {
                if (data.success) {
                    location.reload();

                    // данные сохранены
                } else {
                    // сервер вернул ошибку и не сохранил наши данные
                }
            })
            .fail(function () {
                // не удалось выполнить запрос к серверу
            });

        return false;
    }
</script>
