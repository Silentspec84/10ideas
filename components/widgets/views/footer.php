<?php

?>

<div class="uk-section-default uk-background-secondary uk-margin-large-top">
    <div class="uk-section uk-dark uk-background-cover uk-padding-small">
        <div class="uk-container">
            <div class="uk-grid uk-margin-top">
                <div class="uk-width-1-4">
                    <a class="uk-logo" href="/"><img src="../../web/img/logo2.png" alt="" height="5"></a>
                    <div class="uk-grid uk-padding">
                        <div class="uk-width-1-5">
                            <a href="" style="color:#fc6d26;" class="uk-icon-button" uk-icon="twitter"></a>
                        </div>
                        <div class="uk-width-1-5">
                            <a href="" style="color:#fc6d26;" class="uk-icon-button" uk-icon="google-plus"></a>
                        </div>
                        <div class="uk-width-1-5">
                            <a href="" style="color:#fc6d26;" class="uk-icon-button" uk-icon="tumblr"></a>
                        </div>
                        <div class="uk-width-1-5">
                            <a href="" style="color:#fc6d26;" class="uk-icon-button" uk-icon="facebook"></a>
                        </div>
                        <div class="uk-width-1-5">
                            <a href="" style="color:#fc6d26;" class="uk-icon-button" uk-icon="linkedin"></a>
                        </div>
                    </div>
                </div>

                <div class="uk-width-1-4">
                </div>

                <div class="uk-width-1-4">
                    <div>
                        <ul class="uk-nav uk-nav-default">
                            <li><a style="color:#fc6d26;"><?=Yii::t('common', 'SERVICE')?></a></li>
                            <li><a style="color:#fc6d26;" href="/"><?=Yii::t('common', 'Home page')?></a></li>
                            <li><a style="color:#fc6d26;" href="#"><?=Yii::t('common', 'Reviews')?></a></li>
                            <li><a style="color:#fc6d26;" href="#"><?=Yii::t('common', 'Additional features')?></a></li>
                            <li><a style="color:#fc6d26;" href="#"><?=Yii::t('common', 'Blog')?></a></li>
                        </ul>
                    </div>
                </div>

                <div class="uk-width-1-4">
                    <div>
                        <ul class="uk-nav uk-nav-default">
                            <li><a style="color:#fc6d26;"><?=Yii::t('common', 'HELP')?></a></li>
                            <li><a style="color:#fc6d26;" href="#"><?=Yii::t('common', 'Knowledge base')?></a></li>
                            <li><a style="color:#fc6d26;" href="#"><?=Yii::t('common', 'Tutorials')?></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div>
                <p class="uk-align-right" style="color:#fc6d26;">&copy; 10ideas <?= date('Y') ?></p>
            </div>
        </div>
    </div>
</div>