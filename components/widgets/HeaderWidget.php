<?php

namespace app\components\widgets;

use app\models\UserSettings;
use Yii;
use yii\base\Widget;

/**
 * Виджет отвечает за вывод кастомного хедера
 * Class HeaderWidget
 * @package app\components
 */
class HeaderWidget extends Widget
{
    private $user;

    private $user_settings;

    public function init()
    {
        parent::init();

        if (isset(Yii::$app)) {
            $this->user = Yii::$app->getUser()->identity;
            if ($this->user) {
                $this->user_settings = UserSettings::find()->where(['user_id' => Yii::$app->user->id])->one();
            }
        }
    }

    public function run()
    {
        return $this->render('header', ['user' => $this->user, 'settings' => $this->user_settings]);
    }

}