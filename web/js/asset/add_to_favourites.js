$('#favourite').click(favourites);

function favourites(idea_id){
    var csrfToken = $('meta[name="csrf-token"]').attr("content");
    var group_new = $("#favourite-group-new-" + idea_id).val();
    var group_old = $("#favourite-group-" + idea_id).val();
    var user_id = $("#favourite-user-" + idea_id).val();
    var group;
    if (group_new !== '') {
        group = group_new;
    } else {
        group = group_old;
    }
    $.ajax({
            type: 'POST',
            url: '/main/favourite',
            data: {idea_id: idea_id, group: group, user_id: user_id, _csrf : csrfToken}
        }
    )
        .done (function(data) {
            if (data.success) {
                window.location.reload();
                // данные сохранены
            } else {
                swal("Warning!", "Something goes wrong...", "warning");
                // сервер вернул ошибку и не сохранил наши данные
            }
        })
        .fail(function () {
            swal("Error!", "You have some errors in your idea form!", "error");
            // не удалось выполнить запрос к серверу
        });
}