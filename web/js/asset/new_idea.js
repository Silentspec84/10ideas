$('#new-idea-form').on('beforeSubmit', function () {
    var $yiiform = $(this);
    // отправляем данные на сервер
    $.ajax({
            type: 'POST',
            url: '/new/post-new',
            data: $yiiform.serializeArray()
        }
    )
        .done (function(data) {
            if (data.success) {
                $('#idea-points').text(data.points.idea_points);
                $('#my-idea-points').text(data.points.idea_points);
                $('#my_idea_points').text(data.points.idea_points);
                $('#text').val('');
                if (data.points.idea_points < 1) {
                    $('#card_new').attr('hidden', 'true');
                    swal({title: data.title, text: data.message, icon: "warning",   buttons: {
                            cancel: "Ok",
                            confirm: {
                                text: data.rate,
                                value: "confirm"
                            }
                        }}).then(function(value) {
                        switch (value) {
                            case "confirm":
                                window.location.href = '/today';
                                break;
                            default:
                        }
                    });
                } else {
                    swal({title: data.title, text: data.message, icon: "success", buttons: false, timer: 3000});
                }
                // данные сохранены
            } else {
                swal("Warning!", data['idea-text'][0], "warning");
                // сервер вернул ошибку и не сохранил наши данные
            }
        })
        .fail(function () {
            swal("Error!", "You have some errors in your idea form!", "error");
            // не удалось выполнить запрос к серверу
        });

    return false; // отменяем отправку данных формы
});