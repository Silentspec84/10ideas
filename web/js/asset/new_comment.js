$('#new-comment').click(new_comment);

function new_comment(idea_id){
    var csrfToken = $('meta[name="csrf-token"]').attr("content");
    var text = $("#new-comment-text-" + idea_id).val();
    var user_id = $("#new-comment-user-" + idea_id).val();
    $.ajax({
            type: 'POST',
            url: '/main/comment',
            data: {idea_id: idea_id, text: text, user_id: user_id, _csrf : csrfToken}
        }
    )
        .done (function(data) {
            if (data.success) {
                $('#new-comment-text-' + data.comment.id).text(data.comment.text);
                window.location.reload();
                // данные сохранены
            } else {
                swal("Warning!", "Something goes wrong...", "warning");
                // сервер вернул ошибку и не сохранил наши данные
            }
        })
        .fail(function () {
            swal("Error!", "You have some errors in your idea form!", "error");
            // не удалось выполнить запрос к серверу
        });
}