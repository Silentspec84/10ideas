$('#edit-idea').click(edit);

function edit(id){
    var csrfToken = $('meta[name="csrf-token"]').attr("content");
    var text = $("#edit-text-" + id).val();
    $.ajax({
            type: 'POST',
            url: '/main/edit',
            data: {id: id, text: text, _csrf : csrfToken}
        }
    )
        .done (function(data) {
            if (data.success) {
                $('#text-' + data.idea.id).text(data.idea.text);
                swal({title: data.title, text: data.message, icon: "success", buttons: false, timer: 3000});
                // данные сохранены
            } else {
                swal("Warning!", "Something goes wrong...", "warning");
                // сервер вернул ошибку и не сохранил наши данные
            }
        })
        .fail(function () {
            swal("Error!", "You have some errors in your idea form!", "error");
            // не удалось выполнить запрос к серверу
        });
}