$('#rate').click(rate);

function rate(id, value){
    var csrfToken = $('meta[name="csrf-token"]').attr("content");
    $.ajax({
            type: 'POST',
            url: '/main/rate',
            data: {id: id, value: value, _csrf : csrfToken}
        }
    )
        .done (function(data) {
            if (data.success) {
                if(data.message) {
                    swal(data.title, data.message, "warning");
                } else {
                    $('#idea-points').text(data.points.idea_points);
                    $('#my-idea-points').text(data.points.idea_points);
                    $('#check-points').text(data.points.check_points);
                    $('#my-check-points').text(data.points.check_points);
                    $('#' + data.idea.id).text(data.idea.rating);
                    $('#voted_' + data.idea.id).css({'display': 'inline'});
                }
                // данные сохранены
            } else {
                swal(data.title, data.message, "warning");
                // сервер вернул ошибку и не сохранил наши данные
            }
        })
        .fail(function () {
            swal("Error!", "You have some errors in your idea form!", "error");
            // не удалось выполнить запрос к серверу
        })
}