<?php

/* @var $this yii\web\View */

$this->title = '10 ideas is your best creativity trainer';
?>
<style>
    .my-button {
        background-color:#fc6d26;
    }
    .my-button:hover {
        background-color:#fca326;
    }
</style>
<div class="site-index">
    <div class="container">
        <div class="jumbotron">
            <h1><?=Yii::t('common', 'Creativity Trainer')?></h1>

            <p class="lead"><?=Yii::t('common', 'If you develop this creative skill, then for any problem you can come up with several solutions at once.')?></p>

            <p><?=Yii::t('common', 'You can say that the idea of inventing more than one solution to the problem is not new. Of course. But how often do you do this? How many ideas have you created for the last task you worked with? And how many ideas are there in your "idea bank"?')?></p>
            <?php if (Yii::$app->user->isGuest): ?>
                <p>
                    <a style="" class="uk-button uk-button-danger my-button" href="/user/registration/register"><?=Yii::t('common', 'Try it, it`s free')?></a>
                </p>
            <?php endif; ?>
        </div>
        <div class="body-content">
            <div class="uk-grid uk-margin-large-top">
                <div class="uk-width-1-1">
                    <div class="uk-grid">
                        <div class="uk-width-1-2">
                            <h2 class="uk-text-center"><?=Yii::t('common', 'Habbit to invent ideas')?></h2>
                            <p class="lead"><?=Yii::t('common', 'All you need is to start inventing 10 ideas a day and hold on for at least six months. Forming a sustainable habit of inventing 10 ideas a day can take you from 18 to 243 days.')?></p>
                        </div>
                        <div class="uk-width-1-2">
                            <img src="../../web/img/template_illustratio.svg" class="uk-border-rounded uk-align-right" alt="Idea">
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <div class="uk-grid">
                        <div class="uk-width-1-2">
                            <img src="../../web/img/template_illustratio2.svg" class="uk-border-rounded uk-align-left" alt="Idea">
                        </div>
                        <div class="uk-width-1-2">
                            <h2 class="uk-text-center"><?=Yii::t('common', 'Creativity is a skill')?></h2>
                            <p class="lead"><?=Yii::t('common', 'Creativity is a skill. It can be compared with sports. You will need a trainer, a program and regular workouts.')?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-container uk-background-muted uk-margin-large-top uk-padding-large">
                <div class="uk-grid">
                    <div class="uk-width-1-1">
                        <h2 class="uk-text-center"><?=Yii::t('common', 'Why do you need to develop the skill')?></h2>
                        <p class="lead uk-text-center"><?=Yii::t('common', 'We offer you all the conditions for improving the skills of creativity absolutely for free')?></p>
                    </div>
                </div>
                <div class="row uk-margin-large-top">
                    <div class="uk-grid uk-grid-match">
                        <div class="uk-width-1-1 uk-width-1-3@l uk-margin-bottom">
                            <div class="uk-card uk-card-default">
                                <span style="color: #fca326" class="uk-margin-left uk-margin-top" uk-icon="icon: star; ratio: 2"></span>
                                <div class="uk-card-body uk-padding-small">
                                    <h3 class="uk-card-title uk-text-center"><?=Yii::t('common', '3,000 ideas in one year')?></h3>
                                    <p><?=Yii::t('common', 'Each month you will have 300 or more new ideas, and in a year you will record more than 3,000.80% of the world\'s population does not appear as much in their entire life.')?></p>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-1-1 uk-width-1-3@l uk-margin-bottom">
                            <div class="uk-card uk-card-default">
                                <span style="color: #fca326" class="uk-margin-left uk-margin-top" uk-icon="icon: warning; ratio: 2"></span>
                                <div class="uk-card-body uk-padding-small">
                                    <h3 class="uk-card-title uk-text-center"><?=Yii::t('common', '10 ideas on the fly ability')?></h3>
                                    <p><?=Yii::t('common', 'For each life or work situation, you can come up with 10 ideas on the fly.And where there are several ideas, there is a choice and a solution.')?></p>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-1-1 uk-width-1-3@l uk-margin-bottom">
                            <div class="uk-card uk-card-default">
                                <span style="color: #fca326" class="uk-margin-left uk-margin-top" uk-icon="icon: search; ratio: 2"></span>
                                <div class="uk-card-body uk-padding-small">
                                    <h3 class="uk-card-title uk-text-center"><?=Yii::t('common', '10 is not enough')?></h3>
                                    <p><?=Yii::t('common', 'Over time you will realize that 10 is not enough, and you will begin to invent more, deriving pleasure from it.')?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-grid uk-grid-match uk-margin-large-top">
                        <div class="uk-width-1-1 uk-width-1-3@l uk-margin-bottom">
                            <div class="uk-card uk-card-default">
                                <span style="color: #fca326" class="uk-margin-left uk-margin-top" uk-icon="icon: bolt; ratio: 2"></span>
                                <div class="uk-card-body uk-padding-small">
                                    <h3 class="uk-card-title uk-text-center"><?=Yii::t('common', 'Develop your "idea muscle"')?></h3>
                                    <p><?=Yii::t('common', 'You have no obligation to anyone! You can immediately forget what you recorded. Because the most important thing in this practice is to develop the skill and pump through the "idea muscle."')?></p>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-1-1 uk-width-1-3@l uk-margin-bottom">
                            <div class="uk-card uk-card-default">
                                <span style="color: #fca326" class="uk-margin-left uk-margin-top" uk-icon="icon: happy; ratio: 2"></span>
                                <div class="uk-card-body uk-padding-small">
                                    <h3 class="uk-card-title uk-text-center"><?=Yii::t('common', 'A chance to invent cool idea')?></h3>
                                    <p><?=Yii::t('common', 'But if suddenly you come up with a cool idea, you can try to develop it. It can bring you happiness, joy and even money. And you need to do just one step.')?></p>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-1-1 uk-width-1-3@l uk-margin-bottom">
                            <div class="uk-card uk-card-default">
                                <span style="color: #fca326" class="uk-margin-left uk-margin-top" uk-icon="icon: hashtag; ratio: 2"></span>
                                <div class="uk-card-body uk-padding-small">
                                    <h3 class="uk-card-title uk-text-center"><?=Yii::t('common', 'Motivation')?></h3>
                                    <p><?=Yii::t('common', 'In the first week and a half you will be very interested, then the week of the "loss of the fuse" will come - this is the most difficult time, and you need to find motivation. The best motivation is social participation and approval. Show what you make up and see what others make up.')?></p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>