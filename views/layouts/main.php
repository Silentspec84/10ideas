<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\SweetAlertAsset;
use app\assets\UikitCssAsset;
use app\components\widgets\FooterWidget;
use app\components\widgets\HeaderWidget;
use dektrium\user\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use app\assets\AppAsset;

AppAsset::register($this);
UikitCssAsset::register($this);
SweetAlertAsset::register($this);

$flashes = Yii::$app->session->getAllFlashes();

if ($flashes) {
    foreach ($flashes as $status => $message) {
        $this->registerJs('
        try {
            $.Notification.notify("' . $status . '","top center", "Уведомление системы","' . $message . '" );
        }
        catch(e) {}
       ', View::POS_END);
    }
}

$this->registerMetaTag([
    'name' => 'description',
    'content' => '10 ideas is your creativity trainer. Create your ideas everyday, train your creativity muscle',
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'many ideas, your idea, cool idea, idea muscle, idea, ideas, creativity skill, creative, creativity',
]);

/** @var User $user */
$user = Yii::$app->getUser()->identity;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="og:title" content="10 ideas is a creativity trainer">
    <meta name="og:description" content="your creativity trainer">
    <meta name="og:url" content="your-ten-ideas.com">
    <meta name="og:site_name" content="10 ideas">
    <meta name="og:locale" content="en-EN">
    <meta name="og:image" content="/../../web/img/logo2.png">
    <meta name="og:type" content="website">
    <meta charset="utf-8">
    <meta name="yandex-verification" content="516adbe2d2987516" />
    <link rel="shortcut icon" href="<?=Yii::$app->request->baseUrl;?>/img/favicon.ico" alt="10 ideas is your best creativity trainer" title="10 ideas" type="image/x-icon" />
    <link rel="canonical" href="<?=Yii::$app->request->baseUrl;?>"/>
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <style>
        html {
            height: 100%;
        }
        body {
            display: flex;
            flex-direction: column;
            min-height: 100%;
            margin: 0;
        }
        .main {
            padding: 0px;
            flex-grow: 1;
        }
        .footer {
            padding: 0px;
        }
    </style>
</head>
<body>
<?php $this->beginBody() ?>

<div>
    <div class="uk-margin-xlarge-bottom@s" uk-sticky="sel-target: .uk-navbar-container; cls-active: uk-navbar-sticky">
        <header class="uk-background-muted uk-margin-xlarge-bottom@s">
            <!--Виджет кастомного хедера-->
            <?php HeaderWidget::begin();?>
            <?php HeaderWidget::end();?>
        </header>
    </div>
    <?= $content; ?>
    <footer>
        <!--Виджет кастомного футера-->
        <?php FooterWidget::begin();?>
        <?php FooterWidget::end();?>
    </footer>
</div>
<?php $this->endBody() ?>
</body>
</html>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-48975510-5"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-48975510-5');
    </script>
<?php $this->endPage() ?>