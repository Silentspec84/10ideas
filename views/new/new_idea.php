<?php

use app\assets\NewIdeaAsset;
use app\models\Idea;
use app\models\Lang;
use app\models\UserSettings;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use vova07\imperavi\Widget;

/* @var $this yii\web\View */
/* @var Idea $idea */
/* @var array $sector */

NewIdeaAsset::register($this);

$this->title = Yii::t('common', 'New idea');
?>
<style>
    .my-button {
        background-color:#fc6d26;
    }
    .my-button:hover {
        background-color:#fca326;
    }
</style>
<br class="uk-hidden@m">
<br class="uk-hidden@m">
<br class="uk-hidden@m">
<br class="uk-hidden@m">
<br class="uk-hidden@m">
<br class="uk-hidden@m">
<div class="uk-section uk-inline uk-margin-xlarge-top@s">
    <img src="<?=$sector[date("w", time())]['img']?>" alt="New idea">
    <div class="uk-overlay uk-dark uk-position-bottom">
        <div class="uk-background-muted uk-box-shadow-medium">
            <h1 class="uk-text-center uk-margin-xlarge-top@s"><?=Yii::t('common', 'Today`s topic is ')?> <?=Yii::t('common', $sector[date("w", time())]['title'])?></h1>
            <p class="lead uk-text-center">
                <?=Yii::t('common', 'Formulate and write down your idea in the form below. Be brief - the idea should not be longer than 255 characters. With each new publication of your idea, you spend 1 ideapoint. Their current number can be seen in the top menu on the right. You can fill in idepoints only by voting for other people\'s ideas. Act wisely!')?>
            </p>
        </div>
    </div>
</div>

<div class="site-index">
    <div class="container">
        <div class="body-content">
            <div class="uk-background-muted uk-box-shadow-medium">
                <h3 class="uk-text-center"><?=Yii::t('common', 'You have:')?> <span id="my_idea_points"><?=$user_params['user_settings']['idea_points']?></span> <?=Yii::t('common', 'Idea Points')?></h3>
                <?php if ($user_params['user_settings']['check_points'] > 0): ?>
                    <p class="uk-text-center"><?=Yii::t('common', 'You still have some CheckPoints:')?> <?=$user_params['user_settings']['check_points']?><?=Yii::t('common', '. Rate other users ideas and you`ll get')?> <a href="/today"><?=Yii::t('common', 'additional IdeaPoints!')?></a></p>
                <?php endif; ?>
            </div>
            <div id="card_new" class="uk-card uk-card-body uk-box-shadow-medium" <?php if ($user_params['user_settings']['idea_points'] < 1):?>hidden<?php endif; ?>>
                <article class="uk-comment">
                    <header class="uk-comment-header uk-grid-medium uk-flex-middle" uk-grid>
                        <div class="uk-width-auto">
                            <img class="uk-comment-avatar" src="<?=$user_params['user_settings']['avatar']?>" width="80" height="80" alt="">
                        </div>
                        <div class="uk-width-expand">
                            <h4 class="uk-comment-title uk-margin-remove"><a class="uk-link-reset" href="#"><?=$user_params['user']['username']?></a></h4>
                            <ul class="uk-comment-meta uk-subnav uk-subnav-divider uk-margin-remove-top">
                                <li><?=date("d.m.Y", time())?></li>
                                <li>#<?=Yii::t('common', $sector[date("w", time())]['title'])?></li>
                                <li><img src="<?=$user_params['user_lang']['img_path']?>" style="height:10px;" class="uk-margin-small-right"> <?=$user_params['user_lang']['name']?></li>
                                <li>
                                    <a uk-icon="icon: chevron-down; ratio: 1.5" class="uk-margin-small-right uk-text-danger" >
                                    </a> 0 <a uk-icon="icon: chevron-up; ratio: 1.5" class="uk-margin-small-left uk-text-success">
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </header>

                    <?php $form = ActiveForm::begin([
                        'id' => 'new-idea-form',
                    ]); ?>

                    <?php $idea = new Idea(); ?>

                    <?= $form->field($idea, 'status', ['enableLabel' => false, 'inputOptions' => ['class' => 'uk-select']])->dropdownList([0 => 'Public', 1 => 'Private'])->hiddenInput()->label(false); ?>

                    <?= $form->field($idea, 'user_id', ['inputOptions' => ['value' => $user_params['user']['id']]])->hiddenInput()->label(false);?>

                    <?= $form->field($idea, 'status', ['inputOptions' => ['value' => 0]])->hiddenInput()->label(false);?>

                    <?= $form->field($idea, 'sector_id', ['inputOptions' => ['value' => array_search($sector[date("w", time())], $sector)]])->hiddenInput()->label(false);?>

                    <?php if ($user_params['user_settings']['idea_points'] >= 1):?>
                        <div class="uk-comment-body">
                            <p><?= $form->field($idea, 'text', ['enableLabel' => false, 'inputOptions' => ['class' => 'uk-textarea', 'id' => 'text']])->textArea(['rows' => 3, 'enableLabel' => false]) ?></p>
                        </div>
                    <?php endif; ?>

                </article>

                <?= Html::submitButton(Yii::t('user', Yii::t('common', 'Post idea')), ['class' => 'uk-button uk-button-danger my-button', 'id' => 'subbutton']) ?>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>