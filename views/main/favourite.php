<?php

use app\assets\AddToFavAsset;
use app\models\UserFavourites;

AddToFavAsset::register($this);
?>
<style>
    .my-button {
        background-color:#fc6d26;
    }
    .my-button:hover {
        background-color:#fca326;
    }
</style>
<div id="modal-favourite-<?=$idea['idea']['id']?>" class="modal-sections" uk-modal>
    <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <div class="uk-modal-header">
            <h2 class="uk-modal-title">Добавить в избранное</h2>
        </div>
        <div class="uk-modal-body">
            <input type="text" hidden id="favourite-user-<?=$idea['idea']['id']?>" value="<?=$user_params['user']['id']?>">
            Создать новую группу:
            <input type="text" class="uk-input" id="favourite-group-new-<?=$idea['idea']['id']?>">
            <?php $groups = UserFavourites::getUserFavouriteGroups();?>
            <p>
                Или добавить в существующую:
                <select class="uk-select" id="favourite-group-<?=$idea['idea']['id']?>">
                    <?php foreach($groups as $group): ?>
                        <option value="<?=$group['group_title']?>"><?=$group['group_title']?></option>
                    <?php endforeach; ?>
                </select>
            </p>
        </div>
        <div class="uk-modal-footer uk-text-right">
            <button id="favourite" onclick="favourites(<?=$idea['idea']['id']?>)" class="uk-button uk-button-danger my-button" type="button"><?=Yii::t('user', Yii::t('common', 'Save'))?></button>
        </div>
    </div>
</div>