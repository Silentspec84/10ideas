<?php

use app\assets\NewCommentAsset;
use app\models\Comment;

NewCommentAsset::register($this);

?>
<style>
    .my-button {
        background-color:#fc6d26;
    }
    .my-button:hover {
        background-color:#fca326;
    }
</style>
    <div class="uk-margin-top">
        <button href="#toggle-animation" class="uk-button uk-button-text" type="button" uk-toggle="target: #toggle-animation-<?=$idea['idea']['id']?>; mode: click;"><?=Yii::t('common', 'Comments:');?> <?=$idea['comments']['count']?></button>
        <div id="toggle-animation-<?=$idea['idea']['id']?>" class="uk-card uk-card-default uk-card-body uk-margin-small" hidden>
            <button href="#toggle-new-comment" class="uk-button uk-button-text" type="button" uk-toggle="target: #toggle-new-comment-<?=$idea['idea']['id']?>; mode: click;"><div uk-icon="icon: plus; ratio: 1"></div> Новый комментарий</button>
            <div id="toggle-new-comment-<?=$idea['idea']['id']?>" hidden>
                <?php $comment_model = new Comment(); ?>

                <input type="text" hidden id="new-comment-user-<?=$idea['idea']['id']?>" value="<?=$user_params['user']['id']?>">

                <div class="uk-margin">
                    <textarea class="uk-textarea" rows="3" id="new-comment-text-<?=$idea['idea']['id']?>"></textarea>
                </div>

                <button id="new-comment" onclick="new_comment(<?=$idea['idea']['id']?>)" class="uk-button uk-button-danger my-button" type="button"><?=Yii::t('user', Yii::t('common', 'Save'))?></button>
            </div>
            <hr>
            <?php if (!empty($idea['comments']['comments'])): ?>
                <?php foreach ($idea['comments']['comments'] as $comment): ?>
                    <div class="uk-card uk-card-body uk-box-shadow-medium uk-padding-small uk-margin">
                        <article class="uk-comment">
                            <header class="uk-comment-header uk-grid-medium uk-flex-middle" uk-grid>
                                <div class="uk-width-auto">
                                    <img class="uk-comment-avatar" width="50" height="50" src="/<?=$comment['user_settings']['avatar']?>" alt="">
                                </div>
                                <div class="uk-width-expand">
                                    <h4 class="uk-comment-title"><?=$comment['user']['username']?></h4>
                                    <ul class="uk-comment-meta uk-subnav">
                                        <li><?=date("d.m.Y H:i:s", $comment['comment']['date_created'])?></li>
                                        <li><?=$comment['comment']['rating']?></li>
                                        <li></li>
                                    </ul>
                                </div>
                            </header>
                            <div class="uk-comment-body"><?=$comment['comment']['text']?></div>
                        </article>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>