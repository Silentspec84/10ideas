<?php

use app\assets\EditIdeaAsset;
use app\assets\NewCommentAsset;
use app\assets\RateAsset;
use app\models\Comment;
use app\models\Idea;
use app\models\UserFavourites;

EditIdeaAsset::register($this);
NewCommentAsset::register($this);
RateAsset::register($this);

$fav = UserFavourites::isIdeaInUserFavouriteGroups($idea['idea']['id']);

?>
<style>
    .my-button {
        background-color:#fc6d26;
    }
    .my-button:hover {
        background-color:#fca326;
    }
    a.disabled {
        pointer-events: none; /* делаем ссылку некликабельной */
        cursor: default;  /* устанавливаем курсор в виде стрелки */
        color: #999; /* цвет текста для нективной ссылки */
    }
</style>
<div class="uk-card uk-card-body uk-box-shadow-medium uk-padding-remove uk-margin">
    <article class="uk-comment uk-comment-primary">
        <header class="uk-comment-header uk-grid-medium uk-flex-middle" uk-grid>
            <ul class="uk-iconnav uk-iconnav-vertical uk-margin-left uk-margin-remove-right">
                <li>
                    <a href="#modal-favourite-<?=$idea['idea']['id']?>" uk-toggle class="uk-link-reset <?php if ($fav): ?>disabled<?php endif; ?>">
                        <span <?php if ($fav): ?>style="color: #B22222"<?php endif; ?> uk-icon="icon: bookmark; ratio: 1" uk-tooltip="title: <?=Yii::t('common', 'Add to favourites');?>; pos: right"></span>
                        <?= Yii::$app->view->render('@app/views/main/favourite', ['idea' => $idea, 'user_params' => $user_params]); ?>
                    </a>
                </li>
                <li><div uk-tooltip="title: <?=Yii::t('common', 'To user page');?>; pos: right"><a href="#" uk-icon="icon: user"></a></div></li>
                <li><div uk-tooltip="title: Подробная статистика; pos: right"><a href="#" uk-icon="icon: search"></a></div></li>
                <?php if ($user_params['user']['id'] === $idea['idea']['user_id']): ?>
                    <li>
                        <a href="#modal-view-<?=$idea['idea']['id']?>" uk-toggle class="uk-link-reset">
                            <span class="uk-margin-small-right" uk-icon="icon: pencil; ratio: 1" uk-tooltip="title:<?=Yii::t('common', 'Edit idea');?>; pos: right"></span>
                        </a>
                    </li>
                <?php endif; ?>
            </ul>
            <div class="uk-width-auto uk-margin-remove-left">
                <img class="uk-comment-avatar" src="/<?=$idea['user_settings']['avatar']?>" width="100" height="100" alt="">
            </div>
            <div class="uk-width-expand">
                <h4 class="uk-comment-title uk-margin-remove"><a class="uk-link" style="color: #fca326" href="/userideas/index/<?=$idea['user']['id']?>/1"><span class="uk-margin-small-right" uk-icon="icon: user; ratio: 2" uk-tooltip="<?=Yii::t('common', 'Go to user ideas');?>"></span></a><?=$idea['user']['username']?></h4>
                <ul class="uk-comment-meta uk-subnav uk-subnav-divider uk-margin-remove-top">
                    <li><?=date("d.m.Y H:i:s", $idea['idea']['date_created'])?></li>
                    <li>#<?=Yii::t('common', $sector[$idea['idea']['sector_id']]['title'])?></li>
                    <li><img src="/<?=$idea['lang']['img_path']?>" style="height:10px;" class="uk-margin-small-right"> <?=$idea['lang']['name']?></li>
                    <?php if (!empty($idea['voted_users'][$idea['idea']['id']])) {
                        $tooltip_message = '';
                        foreach ($idea['voted_users'][$idea['idea']['id']] as $voted_user) {
                            $tooltip_message = $tooltip_message . $voted_user['username'] . '<br>';
                        }
                    }
                    ?>
                    <li>
                        <a id="rate" uk-icon="icon: chevron-down; ratio: 1.5" class="uk-margin-small-right uk-text-danger" onclick="rate(<?=$idea['idea']['id']?>, -1)">
                        </a><span <?php if (!empty($tooltip_message)): ?> uk-tooltip="<?=$tooltip_message?>" <?php endif;?> id="<?=$idea['idea']['id']?>"> <?=$idea['idea']['rating']?> </span><a id="rate" uk-icon="icon: chevron-up; ratio: 1.5" class="uk-margin-small-left uk-text-success" onclick="rate(<?=$idea['idea']['id']?>, 1)">
                        </a>
                    </li>
                    <li>
                        <?php if ($idea['voted'] === true): ?>
                            <div>
                                <span class="uk-label uk-label-warning uk-text-lowercase uk-text-emphasis">
                                    <?=Yii::t('common', 'Voted')?>
                                </span>
                            </div>
                        <?php endif; ?>
                    </li>
                </ul>
            </div>
        </header>
        <div class="uk-comment-body">
            <p id="text-<?=$idea['idea']['id']?>"><?=$idea['idea']['text']?></p>
            <?= Yii::$app->view->render('@app/views/main/comments', ['idea' => $idea, 'user_params' => $user_params]); ?>
        </div>
    </article>
</div>

<div id="modal-view-<?=$idea['idea']['id']?>" class="uk-modal-container" uk-modal>
    <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical">

        <article class="uk-comment">
            <?php if ($idea['voted'] === true): ?>
                <div class="uk-margin-small-bottom uk-align-right">
                                    <span class="uk-label uk-label-warning uk-text-lowercase uk-text-emphasis">
                                        <?=Yii::t('common', 'Voted')?>
                                    </span>
                </div>
            <?php endif; ?>
            <header class="uk-comment-header uk-grid-medium uk-flex-middle" uk-grid>
                <div class="uk-width-auto">
                    <img class="uk-comment-avatar" src="/<?=$idea['user_settings']['avatar']?>" width="80" height="80" alt="">
                </div>
                <div class="uk-width-expand">
                    <h4 class="uk-comment-title uk-margin-remove"><a class="uk-link" style="color: #fca326" href="/userideas/index/<?=$idea['user']['id']?>/1"><span class="uk-margin-small-right" uk-icon="icon: user; ratio: 2" uk-tooltip="<?=Yii::t('common', 'Go to user ideas');?>"></span></a><?=$idea['user']['username']?></h4>
                    <ul class="uk-comment-meta uk-subnav uk-subnav-divider uk-margin-remove-top">
                        <li><?=date("d.m.Y H:i:s", $idea['idea']['date_created'])?></li>
                        <li>#<?=Yii::t('common', $sector[$idea['idea']['sector_id']]['title'])?></li>
                        <li><img src="/<?=$idea['lang']['img_path']?>" style="height:10px;" class="uk-margin-small-right"> <?=$idea['lang']['name']?></li>
                        <?php if (!empty($idea['voted_users'][$idea['idea']['id']])) {
                            $tooltip_message = '';
                            foreach ($idea['voted_users'][$idea['idea']['id']] as $voted_user) {
                                $tooltip_message = $tooltip_message . $voted_user['username'] . '<br>';
                            }
                        }
                        ?>
                        <li>
                            <a id="rate" uk-icon="icon: chevron-down; ratio: 1.5" class="uk-margin-small-right uk-text-danger" onclick="rate(<?=$idea['idea']['id']?>, -1)">
                            </a><span <?php if (!empty($tooltip_message)): ?> uk-tooltip="<?=$tooltip_message?>" <?php endif;?> id="<?=$idea['idea']['id']?>"> <?=$idea['idea']['rating']?> </span><a id="rate" uk-icon="icon: chevron-up; ratio: 1.5" class="uk-margin-small-left uk-text-success" onclick="rate(<?=$idea['idea']['id']?>, 1)">
                            </a>
                        </li>
                        <li><span class="uk-margin-small-right" uk-icon="icon: bookmark; ratio: 1" uk-tooltip="<?=Yii::t('common', 'Add to favourites');?>"></span></li>
                    </ul>
                </div>
            </header>
            <div class="uk-comment-body">
                <?php if ($user_params['user']['id'] === $idea['idea']['user_id']): ?>
                    <?php
                    $idea_model = new Idea();
                    $idea_model = $idea['idea'];
                    ?>

                    <div class="uk-margin">
                        <textarea class="uk-textarea" rows="3" id="edit-text-<?=$idea_model->id?>"><?=$idea_model->text?></textarea>
                    </div>

                    <button class="uk-button uk-button-default uk-modal-close" type="button">Отмена</button>
                    <button id="edit-idea" onclick="edit(<?=$idea_model->id?>)" class="uk-button uk-button-danger my-button" type="button"><?=Yii::t('user', Yii::t('common', 'Save'))?></button>

                <?php else: ?>
                    <p><?=$idea['idea']['text']?></p>
                <?php endif; ?>
                <div class="uk-margin-top">
                    <?= Yii::$app->view->render('@app/views/main/comments', ['idea' => $idea, 'user_params' => $user_params]); ?>
                </div>
            </div>
        </article>
    </div>
</div>