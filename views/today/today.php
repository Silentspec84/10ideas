<?php

use app\assets\RateAsset;
use app\models\UserSettings;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var UserSettings $user_settings */
/* @var array $ideas */
/* @var array $sector */

RateAsset::register($this);

echo Html :: csrfMetaTags();
$this->title = Yii::t('common', 'Today');
?>
<style>
    .my-button {
        background-color:#fc6d26;
    }
    .my-button:hover {
        background-color:#fca326;
    }
</style>

<br class="uk-hidden@m">
<br class="uk-hidden@m">
<br class="uk-hidden@m">
<br class="uk-hidden@m">
<br class="uk-hidden@m">
<br class="uk-hidden@m">
<br class="uk-hidden@m">
<br class="uk-hidden@m">
<br class="uk-hidden@m">
<br class="uk-hidden@m">
<br class="uk-hidden@m">
<br class="uk-hidden@m">
<br class="uk-hidden@m">
<br class="uk-hidden@m">
<br class="uk-hidden@m">
<br class="uk-hidden@m">
<br class="uk-hidden@m">
<br class="uk-hidden@m">
<div class="uk-section uk-inline uk-margin-xlarge-top@s">
    <img src="<?=$sector[date("w", time())]['img']?>">
    <div class="uk-overlay uk-dark uk-position-bottom">
        <div class="jumbotron uk-box-shadow-medium">
            <h1 class="uk-text-center"><?=Yii::t('common', 'Today`s topic is ')?> <?=Yii::t('common', $sector[date("w", time())]['title'])?></h1>
            <p class="lead uk-text-center">
                <?=Yii::t('common', 'Below you will find the ideas of all users for today. You can vote for any - raise or lower the rating. For each vote, one Voice will be written off and half of the Idea Point added. Every day the number of points Voice increases by 10. And now support the most original idea for today!')?>
            </p>
            <h3 class="uk-text-center"><?=Yii::t('common', 'Today posted ideas:')?> <?=$ideas_count?></h3>
        </div>
    </div>
</div>

<div class="site-index">
    <div class="container">
        <?php if ($pages > 1): ?>
            <ul class="uk-pagination uk-flex-center" uk-margin>
                <?php if (Yii::$app->controller->actionParams['page'] > 1): ?>
                    <li><a href="/today/index/<?=(Yii::$app->controller->actionParams['page'] - 1)?>"><span uk-pagination-previous></span></a></li>
                <?php endif; ?>
                <?php for ($page = 1; $page <= $pages; $page++): ?>
                    <li><a href="/today/index/<?=$page?>"><span <?php if (Yii::$app->controller->actionParams['page'] == (int)$page): ?>style="color: #fca326" class="uk-text-bold"<?php endif;?>><?=$page?></span></a></li>
                <?php endfor; ?>
                <?php if (Yii::$app->controller->actionParams['page'] < $pages): ?>
                    <li><a href="/today/index/<?=(Yii::$app->controller->actionParams['page'] + 1)?>"><span uk-pagination-next></span></a></li>
                <?php endif; ?>
            </ul>
        <?php endif; ?>
        <div class="body-content">
            <?php foreach ($ideas as $idea): ?>
                <?= Yii::$app->view->render('@app/views/main/card', ['idea' => $idea, 'sector' => $sector, 'user_params' => $user_params]); ?>
            <?php endforeach; ?>
        </div>
        <?php if ($pages > 1): ?>
        <ul class="uk-pagination uk-flex-center" uk-margin>
            <?php if (Yii::$app->controller->actionParams['page'] > 1): ?>
                <li><a href="/today/index/<?=(Yii::$app->controller->actionParams['page'] - 1)?>"><span uk-pagination-previous></span></a></li>
            <?php endif; ?>
            <?php for ($page = 1; $page <= $pages; $page++): ?>
                <li><a href="/today/index/<?=$page?>"><span <?php if (Yii::$app->controller->actionParams['page'] == (int)$page): ?>style="color: #fca326" class="uk-text-bold"<?php endif;?>><?=$page?></span></a></li>
            <?php endfor; ?>
            <?php if (Yii::$app->controller->actionParams['page'] < $pages): ?>
                <li><a href="/today/index/<?=(Yii::$app->controller->actionParams['page'] + 1)?>"><span uk-pagination-next></span></a></li>
            <?php endif; ?>
        </ul>
        <?php endif; ?>
    </div>
</div>