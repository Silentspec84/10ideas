<?php

/* @var $this yii\web\View */

use app\assets\RateAsset;
use app\models\Idea;
use app\models\UserFavourites;

/* @var array $sectors */
/* @var array $ideas */

RateAsset::register($this);

$sectors = Idea::$sector;

$this->title = '10 ideas';
?>
<style>
    .my-button {
        background-color:#fc6d26;
    }
    .my-button:hover {
        background-color:#fca326;
    }
</style>

<br class="uk-hidden@m">
<br class="uk-hidden@m">
<br class="uk-hidden@m">
<br class="uk-hidden@m">
<br class="uk-hidden@m">
<br class="uk-hidden@m">
<div class="uk-section uk-inline uk-margin-xlarge-top@s">
    <img src="../../web/img/sectors/rating.jpg">
    <div class="uk-overlay uk-dark uk-position-bottom">
        <div class="jumbotron uk-box-shadow-medium">
            <h1 class="uk-text-center"><?=Yii::t('common', 'Favourites');?></h1>
            <p class="lead uk-text-center">
            </p>
        </div>
    </div>
</div>

<div class="site-index">
    <div class="container">
        <div class="body-content">
            <div uk-filter="target: .js-filter">
                <div uk-grid>
                    <div class="uk-width-1-4">
                        <?php $groups = UserFavourites::getUserFavouriteGroups();?>
                        <div class="uk-card uk-card-default uk-card-body uk-background-muted uk-box-shadow-medium uk-box-shadow-hover-large uk-margin-top">
                            <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                                <li class="uk-active"><a class="uk-link-reset">Списки</a></li>
                                <li class="uk-nav-divider"></li>
                                <li class="uk-active" uk-filter-control><a href="#">Все</a></li>
                                <li class="uk-nav-divider"></li>
                                <?php foreach ($groups as $group): ?>
                                    <li uk-filter-control="[data-group='<?= $group['group_title']?>']"><a class="uk-button-text" href="#"><?= $group['group_title']?></a></li>
                                    <li class="uk-nav-divider"></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                    <div class="uk-width-3-4">
                        <ul class="js-filter" uk-grid>
                        <?php foreach ($ideas as $idea): ?>
                        <?php $idea_group = UserFavourites::getIdeaFavouriteGroup($idea['idea']['id'])?>
                            <li data-group="<?=$idea_group['group_title']?>"><?= Yii::$app->view->render('@app/views/main/card', ['idea' => $idea, 'sector' => $sector, 'user_params' => $user_params]); ?></li>
                        <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>