<?php

/* @var $this yii\web\View */

use app\assets\RateAsset;
use app\models\Idea;

/* @var array $sectors */

$sectors = Idea::$sector;

$this->title = '10 ideas';
?>
<style>
    .my-button {
        background-color:#fc6d26;
    }
    .my-button:hover {
        background-color:#fca326;
    }
</style>

<br class="uk-hidden@m">
<br class="uk-hidden@m">
<br class="uk-hidden@m">
<br class="uk-hidden@m">
<br class="uk-hidden@m">
<br class="uk-hidden@m">
<div class="uk-section uk-inline uk-margin-xlarge-top@s">
    <img src="../../web/img/sectors/all.jpg" alt="All ideas">
    <div class="uk-overlay uk-dark uk-position-bottom">
        <div class="jumbotron uk-box-shadow-medium">
            <h1 class="uk-text-center"><?=Yii::t('common', 'All ideas');?></h1>
            <p class="lead uk-text-center">
                <?=Yii::t('common', 'Below you will find all ideas of all users. You can vote for this ideas too! There are already:')?><?=$ideas_count?>
            </p>
        </div>
    </div>
</div>

<div class="site-index">
    <div class="container">
        <div class="body-content">
            <?php if ($pages > 1): ?>
                <ul class="uk-pagination uk-flex-center" uk-margin>
                    <?php if (Yii::$app->controller->actionParams['page'] > 1): ?>
                        <li><a href="/all/index/<?=(Yii::$app->controller->actionParams['page'] - 1)?>"><span uk-pagination-previous></span></a></li>
                    <?php endif; ?>
                    <?php for ($page = 1; $page <= $pages; $page++): ?>
                        <li><a href="/all/index/<?=$page?>"><span <?php if (Yii::$app->controller->actionParams['page'] == (int)$page): ?>style="color: #fca326" class="uk-text-bold"<?php endif;?>><?=$page?></span></a></li>
                    <?php endfor; ?>
                    <?php if (Yii::$app->controller->actionParams['page'] < $pages): ?>
                        <li><a href="/all/index/<?=(Yii::$app->controller->actionParams['page'] + 1)?>"><span uk-pagination-next></span></a></li>
                    <?php endif; ?>
                </ul>
            <?php endif; ?>
                <?php foreach ($ideas as $idea): ?>
                    <?= Yii::$app->view->render('@app/views/main/card', ['idea' => $idea, 'sector' => $sector, 'user_params' => $user_params]); ?>
                <?php endforeach; ?>
        </div>
        <?php if ($pages > 1): ?>
            <ul class="uk-pagination uk-flex-center" uk-margin>
                <?php if (Yii::$app->controller->actionParams['page'] > 1): ?>
                    <li><a href="/all/index/<?=(Yii::$app->controller->actionParams['page'] - 1)?>"><span uk-pagination-previous></span></a></li>
                <?php endif; ?>
                <?php for ($page = 1; $page <= $pages; $page++): ?>
                    <li><a href="/all/index/<?=$page?>"><span <?php if (Yii::$app->controller->actionParams['page'] == (int)$page): ?>style="color: #fca326" class="uk-text-bold"<?php endif;?>><?=$page?></span></a></li>
                <?php endfor; ?>
                <?php if (Yii::$app->controller->actionParams['page'] < $pages): ?>
                    <li><a href="/all/index/<?=(Yii::$app->controller->actionParams['page'] + 1)?>"><span uk-pagination-next></span></a></li>
                <?php endif; ?>
            </ul>
        <?php endif; ?>
    </div>
</div>