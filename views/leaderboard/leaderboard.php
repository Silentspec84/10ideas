<?php

/* @var $this yii\web\View */

use app\assets\RateAsset;
use app\models\Idea;

/* @var array $sectors */
/* @var array $ideas */

RateAsset::register($this);

$sectors = Idea::$sector;

$this->title = '10 ideas';
?>
<style>
    .my-button {
        background-color:#fc6d26;
    }
    .my-button:hover {
        background-color:#fca326;
    }
</style>

<br class="uk-hidden@m">
<br class="uk-hidden@m">
<br class="uk-hidden@m">
<br class="uk-hidden@m">
<br class="uk-hidden@m">
<br class="uk-hidden@m">
<div class="uk-section uk-inline uk-margin-xlarge-top@s">
    <img src="../../web/img/sectors/rating.jpg">
    <div class="uk-overlay uk-dark uk-position-bottom">
        <div class="jumbotron uk-box-shadow-medium">
            <h1 class="uk-text-center"><?=Yii::t('common', 'Leaderboard');?></h1>
            <p class="lead uk-text-center">
                <?=Yii::t('common', 'Below you can find rating of ideas of users. The more users likes the idea, the higher its rating.')?>
            </p>
        </div>
    </div>
</div>

<div class="site-index">
    <div class="container">
        <div class="body-content">
            <?php if ($pages > 1): ?>
                <ul class="uk-pagination uk-flex-center" uk-margin>
                    <?php if (Yii::$app->controller->actionParams['page'] > 1): ?>
                        <li><a href="/leaderboard/index/<?=(Yii::$app->controller->actionParams['page'] - 1)?>"><span uk-pagination-previous></span></a></li>
                    <?php endif; ?>
                    <?php for ($page = 1; $page <= $pages; $page++): ?>
                        <li><a href="/leaderboard/index/<?=$page?>"><span <?php if (Yii::$app->controller->actionParams['page'] == (int)$page): ?>style="color: #fca326" class="uk-text-bold"<?php endif;?>><?=$page?></span></a></li>
                    <?php endfor; ?>
                    <?php if (Yii::$app->controller->actionParams['page'] < $pages): ?>
                        <li><a href="/leaderboard/index/<?=(Yii::$app->controller->actionParams['page'] + 1)?>"><span uk-pagination-next></span></a></li>
                    <?php endif; ?>
                </ul>
            <?php endif; ?>
            <div uk-filter="target: .js-filter">

                <div class="uk-grid-small uk-grid-divider uk-child-width-auto" uk-grid>
                    <div>
                        <ul class="uk-subnav uk-subnav-pill" uk-margin>
                            <li class="uk-active" uk-filter-control><a href="#"><?=Yii::t('common', 'All')?></a></li>
                        </ul>
                    </div>
                    <div>
                        <ul class="uk-subnav uk-subnav-pill" uk-margin>
                            <li uk-filter-control="filter: [data-user='my']; group: user"><a href="#"><?=Yii::t('common', 'Only mine')?></a></li>
                            <li uk-filter-control="filter: [data-user='all_users']; group: user"><a href="#"><?=Yii::t('common', 'Other users')?></a></li>
                        </ul>
                    </div>
                    <div>
                        <ul class="uk-subnav uk-subnav-pill" uk-margin>
                            <?php foreach ($sectors as $sect): ?>
                                <li uk-filter-control="filter: [data-sector='<?=Yii::t('common', $sect['title'])?>']; group: sector"><a href="#"><?=Yii::t('common', $sect['title'])?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>

                <ul class="js-filter">
                    <?php foreach ($ideas as $idea_num => $idea): ?>
                        <li data-user="<?php if ($idea['idea']['user_id'] === Yii::$app->user->id): ?>my<?php else:?>all_users<?php endif;?>" data-sector="<?=Yii::t('common', $sector[$idea['idea']['sector_id']]['title'])?>">
                            <div class="uk-card uk-card-body uk-box-shadow-medium uk-padding-remove uk-margin">
                                <article class="uk-comment uk-comment-primary">
                                    <?php if ($idea['voted'] === true): ?>
                                        <div class="uk-margin-small-bottom uk-align-right">
                                            <span class="uk-label uk-label-warning uk-text-lowercase uk-text-emphasis">
                                                <?=Yii::t('common', 'Voted')?>
                                            </span>
                                        </div>
                                    <?php endif; ?>
                                    <?php if ($idea_num < 3 && Yii::$app->controller->actionParams['page'] === 1 || !Yii::$app->controller->actionParams['page']):?>
                                        <div class="uk-margin-small-bottom uk-align-right">
                                            <span class="uk-label uk-text-lowercase">
                                                <?=Yii::t('common', 'Leader: ')?><?=$idea_num + 1?> <?=Yii::t('common', 'place')?>
                                            </span>
                                        </div>
                                    <?php endif; ?>
                                    <header class="uk-comment-header uk-grid-medium uk-flex-middle" uk-grid>
                                        <div class="uk-width-auto">
                                            <img class="uk-comment-avatar" src="<?=$idea['user_settings']['avatar']?>" width="80" height="80" alt="">
                                        </div>
                                        <div class="uk-width-expand">
                                            <h4 class="uk-comment-title uk-margin-remove"><a class="uk-link" style="color: #fca326" href="/userideas/index/<?=$idea['user']['id']?>/1"><?=$idea['user']['username']?></a></h4>
                                            <ul class="uk-comment-meta uk-subnav uk-subnav-divider uk-margin-remove-top">
                                                <li><?=date("d.m.Y H:i:s", $idea['idea']['date_created'])?></li>
                                                <li>#<?=Yii::t('common', $sector[$idea['idea']['sector_id']]['title'])?></li>
                                                <li><img src="<?=$idea['lang']['img_path']?>" style="height:10px;" class="uk-margin-small-right"> <?=$idea['lang']['name']?></li>
                                                <?php if (!empty($idea['voted_users'][$idea['idea']['id']])) {
                                                    $tooltip_message = '';
                                                    foreach ($idea['voted_users'][$idea['idea']['id']] as $voted_user) {
                                                        $tooltip_message = $tooltip_message . $voted_user['username'] . '<br>';
                                                    }
                                                }
                                                ?>
                                                <li>
                                                    <a id="rate" uk-icon="icon: chevron-down; ratio: 1.5" class="uk-margin-small-right uk-text-danger" onclick="rate(<?=$idea['idea']['id']?>, -1)">
                                                    </a><span <?php if (!empty($tooltip_message)): ?> uk-tooltip="<?=$tooltip_message?>" <?php endif;?> id="<?=$idea['idea']['id']?>"> <?=$idea['idea']['rating']?> </span><a id="rate" uk-icon="icon: chevron-up; ratio: 1.5" class="uk-margin-small-left uk-text-success" onclick="rate(<?=$idea['idea']['id']?>, 1)">
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </header>
                                    <div class="uk-comment-body">
                                        <p><?=$idea['idea']['text']?></p>
                                    </div>
                                </article>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
        <?php if ($pages > 1): ?>
            <ul class="uk-pagination uk-flex-center" uk-margin>
                <?php if (Yii::$app->controller->actionParams['page'] > 1): ?>
                    <li><a href="/leaderboard/index/<?=(Yii::$app->controller->actionParams['page'] - 1)?>"><span uk-pagination-previous></span></a></li>
                <?php endif; ?>
                <?php for ($page = 1; $page <= $pages; $page++): ?>
                    <li><a href="/leaderboard/index/<?=$page?>"><span <?php if (Yii::$app->controller->actionParams['page'] == (int)$page): ?>style="color: #fca326" class="uk-text-bold"<?php endif;?>><?=$page?></span></a></li>
                <?php endfor; ?>
                <?php if (Yii::$app->controller->actionParams['page'] < $pages): ?>
                    <li><a href="/leaderboard/index/<?=(Yii::$app->controller->actionParams['page'] + 1)?>"><span uk-pagination-next></span></a></li>
                <?php endif; ?>
            </ul>
        <?php endif; ?>
    </div>
</div>