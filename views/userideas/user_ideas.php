<?php

use app\assets\RateAsset;
use app\models\Idea;
use app\models\UserSettings;
use yii\helpers\Html;
$sectors = Idea::$sector;
/* @var $this yii\web\View */
/* @var UserSettings $user_settings */
/* @var array $ideas */
/* @var array $sector */

RateAsset::register($this);

echo Html :: csrfMetaTags();
$this->title = Yii::t('common', 'User Ideas');
$id = Yii::$app->controller->actionParams['id'];
if (!$id) {
    $id = Yii::$app->user->id;
}
$page = Yii::$app->controller->actionParams['page'];
?>
<style>
    .my-button {
        background-color:#fc6d26;
    }
    .my-button:hover {
        background-color:#fca326;
    }
</style>

<br class="uk-hidden@m">
<br class="uk-hidden@m">
<br class="uk-hidden@m">
<br class="uk-hidden@m">
<br class="uk-hidden@m">
<br class="uk-hidden@m">
<div class="uk-section uk-inline uk-margin-xlarge-top@s">
    <img src="/../../web/img/sectors/myideas.jpg" alt="My ideas">
    <div class="uk-overlay uk-dark uk-position-bottom">
        <div class="jumbotron uk-box-shadow-medium">
            <h1 class="uk-text-center"><?=Yii::t('common', 'All ideas of user')?> <?=$ideas[0]['user']['username']?></h1>
            <p class="lead uk-text-center">
                <?=Yii::t('common', 'Below you can find all ideas of user')?> <?=$ideas[0]['user']['username']?> <?=Yii::t('common', 'for all time. It`s near')?> <?=$ideas_count?>
            </p>
        </div>
    </div>
</div>

<div class="site-index">
    <div class="container">
        <?php if ($pages > 1): ?>
            <ul class="uk-pagination uk-flex-center" uk-margin>
                <?php if (Yii::$app->controller->actionParams['page'] > 1): ?>
                    <li><a href="/userideas/index/<?=$id?>/<?=(Yii::$app->controller->actionParams['page'] - 1)?>"><span uk-pagination-previous></span></a></li>
                <?php endif; ?>
                <?php for ($page = 1; $page <= $pages; $page++): ?>
                    <li><a href="/userideas/index/<?=$id?>/<?=$page?>"><span <?php if (Yii::$app->controller->actionParams['page'] == (int)$page): ?>style="color: #fca326" class="uk-text-bold"<?php endif;?>><?=$page?></span></a></li>
                <?php endfor; ?>
                <?php if (Yii::$app->controller->actionParams['page'] < $pages): ?>
                    <li><a href="/userideas/index/<?=$id?>/<?=(Yii::$app->controller->actionParams['page'] + 1)?>"><span uk-pagination-next></span></a></li>
                <?php endif; ?>
            </ul>
        <?php endif; ?>
        <div class="body-content">
            <?php foreach ($ideas as $idea): ?>
                <?= Yii::$app->view->render('@app/views/main/card', ['idea' => $idea, 'sector' => $sector, 'user_params' => $user_params]); ?>
            <?php endforeach; ?>
        </div>
        <?php if ($pages > 1): ?>
            <ul class="uk-pagination uk-flex-center" uk-margin>
                <?php if (Yii::$app->controller->actionParams['page'] > 1): ?>
                    <li><a href="/userideas/index/<?=$id?>/<?=(Yii::$app->controller->actionParams['page'] - 1)?>"><span uk-pagination-previous></span></a></li>
                <?php endif; ?>
                <?php for ($page = 1; $page <= $pages; $page++): ?>
                    <li><a href="/userideas/index/<?=$id?>/<?=$page?>"><span <?php if (Yii::$app->controller->actionParams['page'] == (int)$page): ?>style="color: #fca326" class="uk-text-bold"<?php endif;?>><?=$page?></span></a></li>
                <?php endfor; ?>
                <?php if (Yii::$app->controller->actionParams['page'] < $pages): ?>
                    <li><a href="/userideas/index/<?=$id?>/<?=(Yii::$app->controller->actionParams['page'] + 1)?>"><span uk-pagination-next></span></a></li>
                <?php endif; ?>
            </ul>
        <?php endif; ?>
    </div>
</div>