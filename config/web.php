<?php

use dektrium\user\Module;

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'language' => 'en-EN',
    'bootstrap' => ['log'],
//    'on beforeRequest' => function ($event) {
//        if(!Yii::$app->request->isSecureConnection){
//            $url = Yii::$app->request->getAbsoluteUrl();
//            $url = str_replace('http:', 'https:', $url);
//            Yii::$app->getResponse()->redirect($url);
//            Yii::$app->end();
//        }
//    },
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            'baseUrl' => '',
            'enableCsrfValidation' => true,
            'enableCookieValidation' => true,
            'cookieValidationKey' => 'cookieValidationKey',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@app/mail',
            'htmlLayout' => 'layouts/html',
            'textLayout' => 'layouts/text',
            'useFileTransport' => false,
            'messageConfig' => [
                'charset' => 'UTF-8',
                'from' => ['lifeadvizer@yandex.ru' => 'LifeAdvizer'],
            ],
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.yandex.ru',
                'username' => 'lifeadvizer@yandex.ru',
                'password' => '09111984-=qwePOI',
                'port' => '465',
                'encryption' => 'ssl',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'flushInterval' => 1,
            'targets' => [ // цели логов
                [
                    'class' => 'yii\log\FileTarget', // будем писать в файл
                    'exportInterval' => 1,  // по умолчанию 1000
                    'levels' => ['info'], // будем писать, когда вызван метод ifno
                    'categories' => ['application'], // будем писать для категории application (передается в метод вторым параметром, 'application' - значение по умолчанию)
                    'logFile' => '@app/logs/success.log', // будем писать в этот файл
                    'logVars' => [], // запишем переменные, перечисленные в этом массиве (например, можно положить сюда $_POST)
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'exportInterval' => 1,  // по умолчанию 1000
                    'levels' => ['error', 'warning'],
                    'categories' => ['application'],
                    'logFile' => '@app/logs/error.log',
                    'logVars' => [],
                ],

                //deploy logs
                [
                    'class' => 'yii\log\FileTarget',
                    'exportInterval' => 10,  // по умолчанию 1000
                    'levels' => ['info'],
                    'categories' => ['deploy'],  // будем писать для категории deploy (если забыть передать категорию, возьмется значение 'application' и логи запишутся не так, как ожидали)
                    'logFile' => '@app/logs/deploy_success.log',
                    'logVars' => [],
                ],
                [
                    'class' => 'yii\log\EmailTarget', // будем слать email, можно еще писать в БД (yii\log\DbTarget) или писать в syslog (yii\log\SyslogTarget)
                    'exportInterval' => 1,  // по умолчанию 1000
                    'levels' => ['error', 'warning'],
                    'categories' => ['deploy'],
                    'message' => [
                        'from' => ['lifeadvizer@yandex.ru'],
                        'to' => ['silentspec84@mail.ru'],
                        'subject' => 'Ошибка деплоя',
                    ],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules'=>[
                '/' => 'site/index',
                '<controller:[-\w]+>/<action:[-\w]+>/<page:\d+>' => '<controller>/<action>',
                '<controller:[-\w]+>/<action:[-\w]+>/<id:\d+>/<page:\d+>' => '<controller>/<action>',
                '<controller:[-\w]+>/<action:[-\w]+>' => '<controller>/<action>',
//                '<action:(all|new|today|leaderboard|index|rate|lang|sitemap|userideas)[/]*>' => 'site/<action>',
                [
                    'pattern' => 'sitemap',
                    'route' => 'site/sitemap',
                    'suffix' => '.xml'
                ],
            ]
        ],
        'authClientCollection' => [
            'class'   => \yii\authclient\Collection::className(),
            'clients' => [
                // here is the list of clients you want to use
                'vkontakte' => [
                    'class'        => 'dektrium\user\clients\VKontakte',
                    'clientId'     => 'vk_client_id',
                    'clientSecret' => 'vk_client_secret',
                ],
                'facebook' => [
                    'class'        => 'dektrium\user\clients\Facebook',
                    'clientId'     => 'facebook_client_id',
                    'clientSecret' => 'facebook_client_secret',
                ],
                'google' => [
                    'class'        => 'dektrium\user\clients\Google',
                    'clientId'     => 'google_client_id',
                    'clientSecret' => 'google_client_secret',
                ],
                'yandex' => [
                    'class'        => 'dektrium\user\clients\Yandex',
                    'clientId'     => 'yandex_client_id',
                    'clientSecret' => 'yandex_client_secret'
                ],
            ],
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@app/views' => [
                        '@dektrium/user/views'
                    ],
                    '@dektrium/user/views' => '@app/views/user'
                ],
            ],
        ],
        'i18n' => [
            'translations' => [
                'common*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                ],
            ],
        ],
    ],
    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
            'enableFlashMessages' => true,
            'enableRegistration' => true,
            'enableConfirmation' => true,
            'enablePasswordRecovery' => true,
            'emailChangeStrategy' => Module::STRATEGY_SECURE,
            'confirmWithin' => 86400,
            'rememberFor' => 1209600,
            'recoverWithin' => 21600,
            'controllerMap' => [
                'registration' => [
                    'class' => \dektrium\user\controllers\RegistrationController::className(),
                    'on ' . \dektrium\user\controllers\RegistrationController::EVENT_AFTER_REGISTER => function ($e) {
                        Yii::$app->response->redirect(array('/'))->send();
                        Yii::$app->end();
                        }
                    ],
                'login' => [
                    'class' => \dektrium\user\controllers\SecurityController::className(),
                    'on ' . \dektrium\user\controllers\SecurityController::EVENT_AFTER_LOGIN => function ($e) {
                        Yii::$app->response->redirect(array('/'))->send();
                        Yii::$app->end();
                    }
                ],
            ],
            'modelMap' => [
                'User' => 'app\models\User',
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
