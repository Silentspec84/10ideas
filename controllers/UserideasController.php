<?php

namespace app\controllers;

use Yii;

class UserideasController extends MainController
{
    public function actionIndex($id = null, $page = null)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/user/security/login');
        }
        if ($id === null) {
            if (!empty($this->user_params['user'])) {
                $id = $this->user_params['user']['id'];
            }
        }
        $ideas_data = $this->getIdeaProcessor($this->user_params['user']['id'])->getIdeas($page, null, null,'date_created', SORT_DESC, $id);
        $ideas = $ideas_data['ideas'];
        $pages = self::getPagesCount($ideas_data['count']);

        return $this->render('user_ideas', ['ideas_count' => $ideas_data['count'], 'pages' => $pages, 'user_params' => $this->user_params, 'ideas' => $ideas, 'sector' => $this->sectors]);
    }
}