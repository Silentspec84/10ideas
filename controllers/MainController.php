<?php

namespace app\controllers;

use app\classes\processors\IdeaProcessor;
use app\models\Comment;
use app\models\Idea;
use app\models\Lang;
use app\models\PostedIdea;
use app\models\UserFavourites;
use app\models\UserSettings;
use Yii;
use yii\web\Controller;
use yii\web\Cookie;
use yii\web\Response;

class MainController extends Controller
{
    protected $idea_processor;

    protected $user_params = [];
    protected $sectors;

    protected $key = 'trnsl.1.1.20190210T152829Z.c23e3657eae974fb.c4462cbe16429c8709720765cfc9633a82d92a66';

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function beforeAction($action) {

        Yii::$app->language = Lang::getCurrent()->url;

        if (Yii::$app->user->identity && !$this->user_params) {
            $this->user_params = [
                'user' => Yii::$app->user->identity,
                'user_settings' => UserSettings::find()->where(['id' => Yii::$app->user->id])->one(),
                'user_lang' => Lang::getCurrent(),
            ];
        }
        if (!$this->sectors) {
            $this->sectors = Idea::$sector;
        }

        if (!empty($this->user_params['user_settings'])) {
            $settings = $this->user_params['user_settings'];
            $settings->addPoints();
        }

        return true;
    }

    protected function getIdeaProcessor($user_id = null) {
        if ($this->idea_processor === null) {
            $this->idea_processor = new IdeaProcessor($user_id);
        }
        return $this->idea_processor;
    }

    protected static function getPagesCount($ideas_count)
    {
        $items_on_page = Yii::$app->params['itemsOnPage'];
        return ceil($ideas_count / $items_on_page);
    }

    public function actionRate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (!Yii::$app->request->isPost) {
            $message = Yii::t('common', 'You have some errors in your idea form!');
            $title = Yii::t('common', 'Warning!');
            return ['success' => false, 'message' => $message, 'title' => $title];
        }
        /** @var UserSettings $settings */
        $settings = UserSettings::find()->where(['user_id' => Yii::$app->user->id])->one();
        if ($settings->check_points === 0) {
            $points = ['idea_points' => $settings->idea_points, 'check_points' => $settings->check_points];
            $message = Yii::t('common', 'You spent all CheckPoints for today! Try it tomorrow.');
            $title = Yii::t('common', 'Sorry!');
            return ['success' => true, 'message' => $message, 'title' => $title, 'points' => $points];
        }
        $post = Yii::$app->request->post();
        /** @var Idea $idea */
        $idea = Idea::find()->where(['id' => $post['id']])->one();
        $posted_ideas = PostedIdea::find()->all();

        /* @var PostedIdea $posted_idea */
        if (!empty($posted_ideas[$idea->id])) {
            foreach ($posted_ideas as $posted_idea) {
                if ($posted_idea->idea_id !== $idea->id) {
                    continue;
                }
                if ($posted_idea->user_id === Yii::$app->user->id) {
                    $points = ['idea_points' => $settings->idea_points, 'check_points' => $settings->check_points];
                    $message = Yii::t('common', 'You have already checked this idea!');
                    $title = Yii::t('common', 'Sorry!');
                    return ['success' => true, 'message' => $message, 'title' => $title, 'points' => $points];
                }
            }
        }

        if ($idea->user_id === Yii::$app->user->id) {
            $points = ['idea_points' => $settings->idea_points, 'check_points' => $settings->check_points];
            $message = Yii::t('common', 'Nice try, but you can`t voute for your idea!');
            $title = Yii::t('common', 'Nice try!');
            return ['success' => true, 'message' => $message, 'title' => $title, 'points' => $points];
        }

        $settings->check_points = $settings->check_points - 1;
        $settings->idea_points = $settings->idea_points + 0.5;
        $settings->save();
        $points = ['idea_points' => $settings->idea_points, 'check_points' => $settings->check_points];
        $idea->rating = $idea->rating + $post['value'];
        $idea->save();
        $posted_idea = new PostedIdea();
        $posted_idea->user_id = Yii::$app->user->id;
        $posted_idea->idea_id = $idea->id;
        $posted_idea->save();
        return ['success' => true, 'points' => $points, 'idea' => $idea];

    }

    public function actionLang()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (!Yii::$app->request->isPost || !Yii::$app->request->post()) {
            return ['success' => false];
        }
        $post = Yii::$app->request->post();

        /** @var Lang $lang */
        $lang = Lang::find()->where(['id' => $post['id']])->one();

        Yii::$app->language = $lang->local;

        if (!Yii::$app->user->isGuest) {
            $settings = UserSettings::find()->where(['user_id' => Yii::$app->user->id])->one();
            $settings->lang_id = $lang->id;
            $settings->save();
        } else {
            $cookies = Yii::$app->response->cookies;
            if ($cookies->has('lang')) {
                $cookies->remove('lang');
            }
            $cookies->add(new \yii\web\Cookie(['name' => 'lang', 'value' => $lang->url]));
        }

        return ['success' => true];
    }
    
    public function actionEdit()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (!Yii::$app->request->isPost || !Yii::$app->request->post()) {
            return ['success' => false];
        }
        $post = Yii::$app->request->post();
        $idea = Idea::findOne($post['id']);
        $idea->text = $post['text'];
        if ($idea->validate()) {
            $idea->save();
            $cache_key = $idea->id . '-' . $this->user_params['user']['id'] . '-' . Lang::getCurrent()->url;
            Yii::$app->cache->set($cache_key, $idea->text);
            $title = Yii::t('common', 'Congratulations!');
            $message = Yii::t('common', 'Your idea was edited sucessfully!');
            return ['success' => true, 'message' => $message, 'title' => $title, 'idea' => $idea];
        }
        return ['success' => false];
    }

    public function actionComment()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (!Yii::$app->request->isPost || !Yii::$app->request->post()) {
            return ['success' => false];
        }
        $post = Yii::$app->request->post();

        $comment = new Comment();
        $comment->user_id = $post['user_id'];
        $comment->idea_id = $post['idea_id'];
        $comment->text = $post['text'];
        $comment->rating = 0;
        if ($comment->validate()) {
            $comment->save();
            $cache_key = $comment->id . '-' . $this->user_params['user']['id'] . '-' . Lang::getCurrent()->url;
            Yii::$app->cache->set($cache_key, $comment->text);
            return ['success' => true, 'comment' => $comment];
        }
        return ['success' => false];
    }

    public function actionFavourite()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (!Yii::$app->request->isPost || !Yii::$app->request->post()) {
            return ['success' => false];
        }
        $post = Yii::$app->request->post();

        $fav = new UserFavourites();
        $fav->user_id = $post['user_id'];
        $fav->idea_id = $post['idea_id'];
        $fav->group_title = $post['group'];

        if ($fav->validate()) {
            $fav->save();
            return ['success' => true, 'fav' => $fav];
        }
        return ['success' => false];
    }
}