<?php

namespace app\controllers;

class AllController extends MainController
{
    public function actionIndex($page = 1)
    {
        $user_id = null;
        if (!empty($this->user_params['user'])) {
            $user_id = $this->user_params['user']['id'];
        }
        $ideas_data = $this->getIdeaProcessor($user_id)->getIdeas($page, null, null,'date_created', SORT_DESC, null);
        $ideas = $ideas_data['ideas'];
        $pages = self::getPagesCount($ideas_data['count']);

        return $this->render('all_ideas', ['ideas_count' => $ideas_data['count'], 'pages' => $pages, 'user_params' => $this->user_params, 'ideas' => $ideas, 'sector' => $this->sectors]);
    }
}