<?php

namespace app\controllers;

class LeaderboardController extends MainController
{
    public function actionIndex($page = 1)
    {
        $cur_timestamp = strtotime(date('d.m.Y', time()));
        $ideas_data = $this->getIdeaProcessor($this->user_params['user']['id'])->getIdeas($page, $cur_timestamp, null,'rating', SORT_DESC, null);
        $ideas = $ideas_data['ideas'];
        $pages = self::getPagesCount($ideas_data['count']);

        return $this->render('leaderboard', ['pages' => $pages, 'ideas' => $ideas, 'sector' => $this->sectors]);
    }
}