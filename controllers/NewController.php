<?php

namespace app\controllers;

use app\models\Idea;
use app\models\UserSettings;
use yii\bootstrap\ActiveForm;
use yii\web\Response;
use Yii;

class NewController extends MainController
{
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/user/security/login');
        }

        return $this->render('new_idea', ['user_params' => $this->user_params, 'sector' => $this->sectors]);
    }

    public function actionPostNew()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $idea = new Idea();
        /** @var UserSettings $user_settings */
        $user_settings = $this->user_params['user_settings'];
        if ($idea->load(Yii::$app->request->post())) {
            if (!$idea->validate() || $user_settings->idea_points < 1) {
                return ActiveForm::validate($idea);
            }

            $user_settings->idea_points = $user_settings->idea_points - 1;
            $user_settings->save();
            if ($user_settings->idea_points < 1) {
                $points = ['idea_points' => $user_settings->idea_points, 'check_points' => $user_settings->check_points];
                if ($user_settings->check_points > 0) {
                    $message = Yii::t('common', 'Your idea was posted, but you spent all IdeaPoints for today! You can vote for ideas of other users and earn some more IdeaPoints.');
                    $title = Yii::t('common', 'Warning!');
                    $rate = Yii::t('common', 'Go to Today page and rate');
                    return ['success' => true, 'message' => $message, 'title' => $title, 'rate' => $rate, 'points' => $points];
                }
                $title = Yii::t('common', 'Sorry!');
                $message = Yii::t('common', 'Your idea was posted, but you spent all IdeaPoints for today! See you tomorrow.');
                return ['success' => true, 'message' => $message, 'title' => $title, 'points' => $points];
            }

            $title = Yii::t('common', 'Congratulations!');
            $message = Yii::t('common', 'You just posted your idea!');
            $points = ['idea_points' => $user_settings->idea_points, 'check_points' => $user_settings->check_points];
            return ['success' => $idea->save(), 'message' => $message, 'title' => $title, 'points' => $points];
        }
        return $this->renderAjax('new_idea', ['idea' => $idea, 'user_settings' => $user_settings]);
    }

}