<?php

namespace app\controllers;

use app\classes\processors\IdeaProcessor;
use app\models\Idea;
use app\models\Lang;
use app\models\PostedIdea;
use app\models\UserSettings;
use app\models\User;
use DOMDocument;
use Yandex\Translate\Exception;
use Yandex\Translate\Translator;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Cookie;
use yii\web\Response;

class SiteController extends MainController
{

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionSitemap()
    {
        $cache = Yii::$app->cache->get('sitemap');

        if ($cache) {
            $xml = $cache;
        } else {
            $xml = file_get_contents('../web/sitemap.xml');
        }
        return $this->renderPartial('sitemap', ['sm' => $xml]);
    }


}
