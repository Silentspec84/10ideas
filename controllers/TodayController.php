<?php

namespace app\controllers;

class TodayController extends MainController
{
    public function actionIndex($page = 1)
    {
        $cur_timestamp = strtotime(date('d.m.Y', time()));
        $user_id = null;
        if (!empty($this->user_params['user'])) {
            $user_id = $this->user_params['user']['id'];
        }
        $ideas_data = $this->getIdeaProcessor($user_id)->getIdeas($page, $cur_timestamp, null,'date_created', SORT_DESC, null);
        $ideas = $ideas_data['ideas'];
        $pages = self::getPagesCount($ideas_data['count']);

        return $this->render('today', ['ideas_count' => $ideas_data['count'], 'pages' => $pages, 'user_params' => $this->user_params, 'ideas' => $ideas, 'sector' => $this->sectors]);
    }
}