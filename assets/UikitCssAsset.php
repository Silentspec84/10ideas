<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class UikitCssAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/uikit.min.css',
    ];
    public $js = [

    ];
    public $jsOptions = ['position' => \yii\web\View::POS_LOAD]; //POS_READY 89
}
